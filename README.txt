==Inquisitor==

A Web-based, multi-user platform for managing test cases during a
software release.

Multiple test engineers would view tests assigned to them, updating
whether the tests fail or pass.

Managers have a summary page, which shows the number of tests yet to be
done, by product and tester.

The release engineer(s) has a page to mark the state transision of the
products: being deployed, being tested, verified, unused.

Administrators have access to the test cases and tester assignments.

Between releases, need functionality to clear pass/fail values, product
states and which products are included in a release.

Dependencies:
 * Python 2.6
 * Sqlite3
 * Jinja2 template engine
   * MarkupSafe

Install commands
Apt: apt-get install sqlite3 python-sqlite3; pip install Jinja2
Yum: yum install sqlite3 python-sqlite3 pip; pip install Jinja2

This could be set up either as a cgi based system under httpd or its own
http server.

A command-line or curses-based front-end may be desirable.

To view the documentation without running the full program, on the
command-line, run:
    ./py/docs.py

and open a web browser to the address show in the output.  You may also
specify options for the port to open or a configuration file.

Michael P. Reilly (c) 2016 All rights reserved
