// Michael P. Reilly (c) 2016 All rights reserved
// inquisitor.static.check_passwords.js

function check_passwords() {
    var p1 = document.forms[0].password1.value;
    var p2 = document.forms[0].password2.value;
    if (p1 != p2) {
        alert("Password mismatch");
        return false;
    } else {
        return true;
    }
}
