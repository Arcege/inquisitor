// Michael P. Reilly (c) 2016 All rights reserved.
// inquisitor.static.perms.js
// Dynamically display permissions based on roles
// selected in the form.
//
//

function role_click() {
  perms.length = 0;  // clear out the array
  var form = document.all.userform;
  var roles = new Array;
  var el = form.getElementsByTagName('input');
  for (var i = 0, len=el.length; i<len; i++) {
    if (el[i].type == 'checkbox' &&
        el[i].name == 'role' &&
        el[i].checked) {
      roles.push(el[i].value);
    }
  }
  for (role in roles) {
    var theseroles = allperms[roles[role]];
    for (perm in theseroles) {
      if (perms.indexOf(theseroles[perm]) == -1) {
         perms.push(theseroles[perm])
      }
    }
  }
  displayperms();
  return true;
}

function displayperms() {
  var span = document.getElementById("perms");
  perms.sort();
  span.innerHTML = perms.join(", ");
}

