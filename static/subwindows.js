// Michael P. Reilly (c) 2016 All rights reserved.
// inquisitor.static.subwindows.js
// When opening a window, keep track so we can close them later.
//

function openwindow(url, name, params) {
    var win = window.open(url, name, params);
    var subwindows = sessionStorage.subwindows;
    if (subwindows == undefined)
        subwindows = [];
    subwindows.push(win);
    sessionStorage.subwindows = subwindows;
    return false;
}

function sub_logout() {
    var win;
    win = window.open("", "inq_testinfo", "");
    if (win)
        win.close()
    win = window.open("", "inq_help", "");
    if (win)
        win.close()
    document.cookie = "sessionid=";
}

function open_testinfo(tid) {
    window.open("testinfo?tid=" + tid, "inq_testinfo",
            "height=500,width=600");
    return false;
}

function open_help(page) {
    window.open(page, "inq_help",
            "height=1000,width=800");
    return false;
}
