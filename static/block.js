// Michael P. Reilly (c) 2016 All rights reserved
// inquisitor.static.block.js

function toggleblock(name) {
    var n = "prod_" + name + "_text";
    var e = document.getElementById(n);
    if (e.style.display == "none") {
        e.style.display = "block";
    } else {
        e.style.display = "none";
    }
    return false;  // so we don't jump in the window on click
}
