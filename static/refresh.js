// Michael P. Reilly (c) 2016 All rights reserved.
// inquisitor.static.refresh.js

// put refresh_dostate in the browser's sessionStorage
if (sessionStorage.refresh_dostate == null)
    sessionStorage.refresh_dostate = "true";
var refresh_timeout = 40;
var refresh_timer = refresh_timeout;
var refresh_reset_count = 0;

// change the state, dostate == false, means no refresh_auto
function refresh_toggle() {
    // do not use explicit boolean logic since sessionStorage could return a string
    sessionStorage.refresh_dostate = (sessionStorage.refresh_dostate == "false") ? "true" : "false";
    refresh_reset();
    refresh_update_stats();
}

function refresh_update_stats() {
    // change the navsidebar button
    var elem = document.getElementById('nav_refresh');
    if (sessionStorage.refresh_dostate == "true") {
        elem.classList.add('refreshon');
        elem.classList.remove('refreshoff');
    } else {
        elem.classList.add('refreshoff');
        elem.classList.remove('refreshon');
    }
    document.getElementById('refresh_stats_refresh_timer').innerText = refresh_timer;
    document.getElementById('refresh_stats_reset_counter').innerText = refresh_reset_count;
    document.getElementById('refresh_stats_dostate').innerText = sessionStorage.refresh_dostate;
}

function refresh_reset() {
    refresh_timer = refresh_timeout;
    refresh_reset_count = 0;
    refresh_update_stats();
}

function refresh_reset_timer() {
    refresh_timer = refresh_timeout;
    refresh_reset_count += 1;
    refresh_update_stats();
}

function refresh_auto() {
    if (sessionStorage.refresh_dostate == "false")
        return;
    refresh_reset_count = 0;
    refresh_timer -= 1;
    if (refresh_timer <= 0) {
        // using location.reload will keep POST methods when we want GET
        window.location = window.location.href;
    }
    refresh_update_stats();
}

function refresh_start() {
    refresh_reset();
    if (window.addEventListener) {
        window.addEventListener("mousemove", refresh_reset_timer, false);
        window.addEventListener("keypress", refresh_reset_timer, false);
    } else if (window.attachEvent) {
        window.attachEvent("onmousemove", refresh_reset_timer);
        window.attachEvent("onkeypress", refresh_reset_timer);
    } else {
        window.onmousemove = refresh_reset_timer;
        window.onkeypress = refresh_reset_timer;
    }
    window.setInterval("refresh_auto();", 1000);
    document.getElementById("nav_refresh").style.visibility = "visible";
}

if (window.addEventListener) {
    window.addEventListener("load", refresh_start, false);
} else if (window.attachEvent) {
    window.attachEvent("onload", refresh_start);
} else {
    window.onload = refresh_start;
}
