#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.query
#

try:
    import urllib.parse as urlparse
except ImportError:
    import urlparse

class Query(dict):
    """Hold incoming query values, similar to cgi.Fieldstorage."""
    def load(self, qs):
        if isinstance(qs, bytes):
            qs = qs.encode('utf-8')
        query = urlparse.parse_qs(qs)
        for i in query:
            self[i] = self.Item(i, query[i])
    def get(self, name, default=None):
        f = super(Query, self).get(name, default)
        if f is not default:
            if f.seq is None:
                return f.value
            else:
                return f.seq
        else:
            return f
    # replicate the interface from cgi.FieldStorage
    getvalue = get
    def getfirst(self, name, default=None):
        f = super(Query, self).get(name, default)
        if f == default:
            return f
        elif f.seq is not None:
            return f.seq[0].value
        else:
            return f.value
    def getlist(self, name):
        f = super(Query, self).get(name)
        if f is None:
            return []
        elif f.seq is not None:
            return [i.value for i in f.seq]
        else:
            return [f.value]
    class Item(object):
        def __init__(self, name, value):
            self.name = name
            if isinstance(value, str):
                self.seq = None
                self.value = value
            elif len(value) == 1:
                self.seq = None
                self.value = value[0]
            else:
                self.seq = [self.__class__(name, i) for i in value]
                self.value = None
        def __str__(self):
            if self.seq is not None:
                return str([str(i) for i in self])
            else:
                return str(self.value)
        def __repr__(self):
            return '%s=%s' % (self.name, repr(self.seq or self.value))
            #return str(self)
        def __len__(self):
            if self.seq:
                return len(self.seq)
            else:
                return 1
        def __getitem__(self, pos):
            if self.seq:
                return self.seq[pos]
            elif pos == 0:
                return self
            else:
                raise IndexError(pos)
        def __iter__(self):
            if self.seq:
                return iter(self.seq)
            else:
                return iter([self])

