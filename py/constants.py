#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.constants
#

encoding = 'utf-8'

# the possible permission values
AllPerms = (
    'ADMIN', 'ASSIGN', 'DATA', 'ISSUES', 'MASTER',
    'NONE', 'PASSFAIL', 'RELEASE', 'ROLES', 'SUDO', 'USERS'
)

# database tables (for stats)
TableNames = (
    'assignment', 'forget', 'issue', 'oplog', 'passfail', 'password', 'perm',
    'productinfo', 'release', 'role', 'session', 'testcase', 'user',
    'version',
)
TableNamesWRelease = (
    'assignment', 'issue', 'oplog', 'passfail', 'release',
)
