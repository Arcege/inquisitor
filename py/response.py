#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.response
#

import time

class Response(object):
    """Catch-all return object with data to send back to client."""
    def __init__(self, template=None, params=None, redirect=None,
            error=None, static=None, ctype=None, page=None, etag=None):
        self.template = template
        self.params = params or {}
        self.redirect = redirect
        self.error = error
        self.static = static
        self.ctype = ctype
        self.page = page
        self.etag = etag
        self.params['currenttime'] = time.asctime()
    def __repr__(self):
        s = []
        if self.template:
            s.append('t=%s' % self.template)
        if self.redirect:
            s.append('r=%s' % self.redirect)
        if self.error:
            s.append('e=%s' % self.error)
        if self.static:
            s.append('s=%s' % self.static)
        if self.ctype:
            s.append('c=%s' % self.ctype)
        if self.params:
            r = str(self.params)
            if len(r) > 37:
                r = '%s...' % r[:37]
            s.append('p=%s' % r)
        return 'R(%s)' % ', '.join(s)

