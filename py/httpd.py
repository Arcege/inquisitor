#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.httpd
#

try:
    import http.server as BaseHTTPServer
except ImportError:
    import BaseHTTPServer
import logging
import optparse
import os
import signal
try:
    import socketserver as SocketServer
except ImportError:
    import SocketServer
import ssl
import subprocess
import sys
import threading
import time

import inquisitor  # load this way to allow for reload on signal
import config
import inqlogging
import query

class Handler(BaseHTTPServer.BaseHTTPRequestHandler, inquisitor.Inquisitor):
    server_type = 'httpd'

    def __init__(self, *args, **kwargs):
        global config
        inquisitor.Inquisitor.__init__(self, config)
        BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, *args, **kwargs)

    def signal_reload(self):
        os.kill(os.getpid(), signal.SIGUSR1)
    def signal_shutdown(self):
        os.kill(os.getpid(), signal.SIGTERM)
    def signal_restart(self):
        os.kill(os.getpid(), signal.SIGHUP)

    def log_message(self, format, *args):
        self.logger.error(format, *args)

    def get_cookie_string(self):
        return self.headers.get('cookie', '')

    def do_POST(self):
        self.query = query.Query()
        if '?' in self.path:
            self.path, q = self.path.split('?', 1)
            self.query.load(q)
        clen = self.headers.get('content-length', None)
        ctype = self.headers.get('content-type', None)
        if ctype is None:
            self.send_error(411)
            return
        elif ctype == 'application/x-www-form-urlencoded':
            qs = self.rfile.read(int(clen))
            self.query.load(qs)
            # XXX for debugging only
            self.logger.debug('POST %s query = %s', self.path, self.query)
        else:
            self.send_error(415, 'unsupported media type: %s' % ctype)
            return
        self.request_handler()

    def do_HEAD(self):
        self.query = query.Query()
        if '?' in self.path:
            self.path, q = self.path.split('?', 1)
            self.query.load(q)
        # XXX for debugging only
        self.logger.debug('HEAD %s query = %s', self.path, self.query)
        self.request_handler()

    def do_GET(self):
        self.query = query.Query()
        if '?' in self.path:
            self.path, q = self.path.split('?', 1)
            self.query.load(q)
        # XXX for debugging only
        self.logger.debug('GET %s query = %s', self.path, self.query)
        self.request_handler()

    def send_error(self, code, message=None):
        try:
            from http.server import _quote_html
        except ImportError:
            from BaseHTTPServer import _quote_html
        try:
            short, long = self.responses[code]
        except KeyError:
            short, long = '???', '???'
        message = message or short
        explain = long
        self.log_error("code %d, message %s", code, message)
        content = (self.error_message_format %
                {'code': code, 'message': _quote_html(message), 'explain': explain})
        self.send_response(code, message)
        self.send_header('Content-type', self.error_content_type)
        if code in (204, 304):
            self.send_header('Connection', self.want_keep_alive)
        else:
            self.send_header('Connection', 'close')
        self.end_headers()
        if self.command != 'HEAD' and code >= 200 and code not in (204, 304):
            self.wfile.write(content)


class SingleHttpd(BaseHTTPServer.HTTPServer):   # for test single threaded
    force_conn_close = True

class Httpd(SocketServer.ThreadingMixIn, BaseHTTPServer.HTTPServer):
    # see ThreadingMixIn
    daemon_threads = True
    force_conn_close = False

def daemonize():
    global config
    import atexit, resource
    cdir = logfile = pidfile = umask = None
    if 'dir.app' in config and config['dir.app']:
        cdir = config['dir.app']
    if 'file.log.access' in config and config['file.log.access']:
        logfile = os.path.join(cdir or os.curdir,
                               config['dir.log'] or os.curdir,
                               config['file.log.access'])
    if 'file.pid' in config and config['file.pid']:
        pidfile = os.path.join(cdir or os.curdir, config['file.pid'])
    if 'sys.umask' in config and config['sys.umask']:
        umask = int(config['sys.umask'], 8)
    maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
    if maxfd == resource.RLIM_INFINITY:
        maxfd = 1024
    try:
        dpid = os.fork()
    except OSError:
        t, e, tb = sys.exc_info()
        raise Exception('%s [%d]' % (e.stderror, e.error))
    if dpid == 0:  # child
        os.setsid()
        try:
            cpid = os.fork()
        except OSError:
            t, e, tb = sys.exc_info()
            raise Exception('%s [%d]' % (e.stderror, e.errno))
        if cpid == 0: # child of child
            os.chdir(cdir or os.curdir)
            os.umask(umask or 000)
        else:
            os._exit(0)  # child
    else:
        os._exit(0)  # parent
    # now we are the second child
    logging.warn('pid = %d', os.getpid())
    if pidfile is not None:
        open(pidfile, 'wt').write('%s\n' % os.getpid())
        # remove the file when we finish
        atexit.register(os.remove, pidfile)
    for i in range(3, maxfd):
        try:
            os.close(i)
        except OSError:
            pass
    if logfile:
        fd = os.open(logfile, os.O_RDWR|os.O_CREAT|os.O_APPEND)
    elif os.isatty(2):
        fd = os.open(os.devnull, os.O_RDWR|os.O_APPEND)
    else:
        fd = 2  # keep 2 (stderr) open
    os.dup2(fd, 0)
    os.dup2(fd, 1)
    if fd != 2:
        os.dup2(fd, 2)
        os.close(fd)

class Termination(object):
    EXIT = signal.SIGTERM
    RESTART = signal.SIGHUP
    RELOAD = signal.SIGUSR1
    class Terminate(Exception):
        pass
    def __init__(self):
        self.lock = threading.Lock()
        self.value = None
    def set(self, value):
        with self.lock:
            self.value = value
    def get(self):
        with self.lock:
            return self.value

class SignalHandler(object):
    def __init__(self, server, event, daemon=False):
        self.server = server
        self.event = event
        self.register(signal.SIGHUP, self.handler)
        self.register(signal.SIGTERM, self.handler)
        self.register(signal.SIGINT, self.handler)
        self.register(signal.SIGUSR1, self.handler)
        #self.register(signal.SIGQUIT, self.terminate)
        if daemon:
            self.register(signal.SIGTSTP, signal.SIG_IGN)
            self.register(signal.SIGTTIN, signal.SIG_IGN)
            self.register(signal.SIGTTOU, signal.SIG_IGN)
    def register(self, signo, method):
        signal.signal(signo, method)
    def handler(self, signo, frame):
        logging.error('Signal received: %d', signo)
        self.server.shutdown()
        self.server.server_close()
        if signo in (signal.SIGHUP,):
            self.event.set(self.event.RESTART)
        elif signo in (signal.SIGTERM, signal.SIGINT):
            self.event.set(self.event.EXIT)
        elif signo in (signal.SIGUSR1,):
            self.event.set(self.event.RELOAD)

def main(config):
    global shutdownevent  # for use in Inquisitor subclass
    inqlogging.start_logging(config)
    addr = (config['net.addr'], int(config['net.port']))
    if config['sys.threading'].lower() == 'true':
        httpd = Httpd(addr, Handler)
    else:
        httpd = SingleHttpd(addr, Handler)
    if config.get('net.ssl', 'false').lower() == 'true':
        try:
            certfile = config['file.cert']
        except KeyError:
            raise SystemExit('missing cert file for SSL')
        certfile = os.path.join(config['dir.app'], certfile)
        logging.warn('starting SSL using %s', certfile)
        httpd.socket = ssl.wrap_socket(httpd.socket,
                certfile=certfile,
                server_side=True,
        )
    httpd.starttime = time.time()  # current time, when we started
    logging.debug('starttime = %s', time.ctime(httpd.starttime))
    shutdownevent = Termination()
    signalhandler = SignalHandler(httpd, shutdownevent, daemon=opts.daemon)
    server_thread = threading.Thread(target=httpd.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    logging.warn('started as pid %s', os.getpid())
    # unfortunately, Python2's handling of threads and signals does not allow
    # the main thread to wait on an event or a join; we need to run a polling
    # event loop to handle this
    while True:
        s = shutdownevent.get()
        if s is not None:
            logging.warn('shutdown')
            break
        time.sleep(0.5)
    server_thread.join()
    raise shutdownevent.Terminate(s)


option_parser = optparse.OptionParser()
option_parser.add_option('--config', '-C', metavar='FILE',
        help='configuration file, after %s' % config.Config.filename)
option_parser.add_option('--daemon', '-d', action='store_true',
        help='run as a daemon in the background')
option_parser.add_option('--logfile', '-L', metavar='FILE',
        help='logging output')
option_parser.add_option('--port', '-p', metavar='INT', type='int',
        help='http or https port to use')
option_parser.add_option('--ssl', action='store_true',
        help='use SSL instead basic http')
option_parser.add_option('--certfile', metavar='FILE',
        help='certificate file')
option_parser.add_option('--nothreading', action='store_false',
        dest='threading', default=True, 
        help='do not start threading')

if __name__ == '__main__':
    config = config.Config()

    opts, args = option_parser.parse_args()
    if opts.config:
        config.load(opts.config)

    if opts.port:
        config['net.port'] = opts.port
    if 'net.port' not in config or not config['net.port']:
        raise SystemExit('expecting port number')
    if opts.ssl:
        config['net.ssl'] = 'true'
    if opts.certfile:
        config['file.cert'] = opts.certfile
    if not opts.threading:
        config['sys.threading'] = 'false'

    # validate the values in config before we start anythin

    config.check_paths()  # raises AssertionError
    try:
        int(config['net.port'])
    except (ValueError, TypeError):
        raise AssertionError('expecting number for net.port')
    if config['sys.umask'] is not None:
        try:
            int(config['sys.umask'], 8)
        except (ValueError, TypeError):
            raise AssertionError('expecting octal for sys.umask')
    assert config['net.ssl'].lower() in ('false', 'true'), \
            'expecting boolean for net.ssl'
    assert config['sys.daemon'].lower() in ('false', 'true'), \
            'expecting boolean for sys.daemon'
    assert config['sys.profiling'].lower() in ('false', 'true'), \
            'expecting boolean for net.ssl'

    # call this before we get sockets
    if (opts.daemon or
        ('sys.daemon' in config and
         config['sys.daemon'].lower() == 'true')):
        daemonize()
    elif config['file.log']:
        logfile = os.path.join(
                config['dir.app'], config['dir.log'] or os.curdir,
                config['file.log']
        )
        sys.stderr = open(logfile, 'at', 1)

    while True:
        try:
            if config['sys.profiling'].lower() == 'true':
                try:
                    import cProfile as profile
                except ImportError:
                    import profile
                profile.run('main(config)', filename='httpd.stats')
            else:
                main(config)
        except Termination.Terminate:
            t, e, tb = sys.exc_info()
            if e.args[0] == Termination.EXIT:
                break
            elif e.args[0] == Termination.RESTART:
                # spawn a new process and exit, should work in Windows and OSX too
                subprocess.Popen(sys.argv)
                break
            elif e.args[0] == Termination.RELOAD:
                logging.warn('reloading configs')
                config.clear()
                if os.path.exists(config.filename):
                    config.load(config.filename)
                if opts.config:
                    config.load(opts.config)

