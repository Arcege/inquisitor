#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.inquisitor
#

try:
    import http.cookies as Cookie
except ImportError:
    import Cookie
import hashlib
import logging
import os
import socket
import sqlite3
import sys
import time
import traceback
import zlib

from jinja2 import Environment, FileSystemLoader

import constants
from release import version
from hashing import gen_etag, genpassword, hashpassword
from inqlogging import SessionAdapter
from session import Session
from response import Response

# this is a superclass for the different interfaces: cgi, httpd, unittest, etc.
class Inquisitor(object):
    protocol_version = 'HTTP/1.1'
    server_version = 'Inquisitor/%s' % version
    encoding = constants.encoding
    tenv = None
    logger = None

    safe_pages = ('', 'help', 'static', 'test', 'favicon.ico')
    login_pages = ('forget', 'login', 'password')

    def __init__(self, config):
        self.config = config
        self.basepath = config['net.path']
        if self.basepath and not self.basepath.startswith('/'):
            self.basepath = '/%s' % self.basepath
        logging.warn('using root path of "%s"', self.basepath)
        appdir = config['dir.app']
        distdir = config['dir.dist']
        self.database_filename = os.path.join(
            appdir,
            config['dir.database'] or os.curdir,
            config['file.database'])
        self.staticdir = os.path.join(
                distdir,
                config['dir.static'])
        self.templatedir = os.path.join(
                distdir,
                config['dir.templates'])
        self.logger = logging.getLogger(None)

    def send_response(self, code, msg=None):
        raise NotImplemented

    def send_header(self, name, value):
        self.wfile.write('%s: %s\n' % (name, value))
    def end_headers(self):
        self.wfile.write('\n')
    def send_error(self, error, page=None):
        self.send_response(error, page)
        self.send_header('Content', 'text/plain')
        self.end_headers()

    root_redirect = '/main'

    def request_handler(self):
        if self.server and self.server.force_conn_close:
            self.want_keep_alive = 'close'
        else:
            self.want_keep_alive = 'keep-alive'
        self.cookies = Cookie.SimpleCookie(self.get_cookie_string())
        # for robots.txt, we cannot use 302, so pass through below
        if self.path in ('', '/', '/favicon.ico', 'favicon.ico'):
            self.send_response(302)
            if self.path == '/favicon.ico':
                self.send_header('Location', '/static/favicon.ico')
            else:
                self.send_header('Location', self.basepath + self.root_redirect)
            if len(self.cookies) > 0:
                self.send_header('Set-Cookie', self.cookies.output(header=''))
            self.end_headers()
            return
        self.referer = self.headers.get('Referer', None)
        if self.referer and '?' in self.referer:
            self.referer, rest = self.referer.split('?', 1)
            del rest
        if self.path == '/robots.txt':
            response = Response(static='robots.txt', ctype='text/plain')
        else:
            response = self.process()
        if response is None:
            self.send_response(204, 'No response')
            self.end_headers()
        elif response.error:
            self.reply_error(response)
        elif response.redirect:
            self.reply_redirect(response)
        elif response.static:
            self.reply_static(response)
        else:
            self.reply_page(response)

    def process(self):
        # don't open the database connection before we are fully ready
        self.db = sqlite3.connect(self.database_filename)

        # first we strip off any basepath, and add it later on redirects
        if self.path.startswith(self.basepath + '/'):
            self.path = self.path.replace(self.basepath + '/', '/')
        assert self.path.startswith('/')
        parts = self.path.lstrip('/').split('/')
        name = parts[0]
        self.parts = parts[1:]
        self.session = None
        if name not in self.safe_pages:
            # send to error page if no database
            try:
                self.db.execute('select 1 from version;')
            except sqlite3.OperationalError:
                self.logger.error('Database not populated')
                return Response(error=500, page='No database')
            Session.purge(self.db, self.config)
        # sessions are generated in the call_login method, this is to
        # validate the session
        if 'sessionid' in self.cookies:
            try:
                sessionid = self.cookies['sessionid'].value
                self.session = Session.retrieve(self.db, sessionid)
                client, clientip = self.get_clientip()
                if self.session.ipaddr != clientip:
                    self.logger.error('session does not match ip')
                    self.logger.debug('session id =%s, client ip =%s',
                            self.session.ipaddr, clientip)
                    self.invalidate_cookie('sessionid')
                    # invalidate the session on our side
                    self.session.clear()
                    self.session = None
                else:
                    m = self.cookies['sessionid']
                    m['path'] = self.basepath or '/'
                    m['domain'] = client
            except KeyError:
                # sessionid is not in the database, so we redirect to
                # the login page below
                self.session = None
            else:
                if not isinstance(self.logger, SessionAdapter):
                    self.logger = SessionAdapter(self.logger,
                            {'sessionid': self.session and self.session.user})
        if (name not in (self.login_pages + self.safe_pages) and
            (self.session is None or self.session.user is None)):
            return Response(redirect=self.basepath + '/login')
        self.roles, self.perms = self.get_perms()
        methodname = 'call_' + name
        try:
            method = getattr(self, methodname)
        except AttributeError:
            self.logger.error('invalid path: %s; no method %s', name, methodname)
            return Response(error=404)
        else:
            self.logger.info('%s(%s)', methodname, sanitize(self.query))
            try:
                response = method()
                self.logger.debug('response = %s', response)
                try:
                    self.valid_session_data()
                except ValueError:
                    t, e, tb = sys.exc_info()
                    if e.args[0] == 'user':
                        response = Response(redirect='logout')
                    elif e.args[0] == 'release':
                        self.session.release = None
                        response = Response(redirect='release')
                # add basepath back on redirects
                if response.redirect and self.basepath:
                    response.redirect = '%s/%s' % (
                        self.basepath, response.redirect.lstrip('/')
                    )
                if response.template:
                    params = response.params
                    params['root'] = self.root_redirect
                    params['basepath'] = self.basepath
                    params['VersionStr'] = 'Inquisitor %s' % version
                    if self.session is not None and self.session.user is not None:
                        params['username'] = self.session.sudo or self.session.user
                    if self.session is not None and self.session.release is not None:
                        params['release'] = self.session.release
                    if self.session and self.session.sudo:
                        params['sudo'] = True
                    params['userroles'] = self.roles
                    params['userperms'] = self.perms
                    if 'referer' not in params:
                        params['referer'] = self.referer
                    # handle displaying errors:
                    if self.session is not None and self.session.error is not None:
                        params['error'] = self.session.error
                        self.session.error = None
                    params['template'] = response.template
                    if isinstance(response.template, str):
                        template = self.get_template(response.template)
                        tname = response.template
                    else:
                        template = response.template
                        tname = os.path.basename(template.filename)
                        params['template'] = os.path.splitext(tname)[0]
                    etag = gen_etag(template.filename, params)
                    if ('if-none-match' in self.headers and
                        not tname.startswith('smoketest')):
                        inm = self.headers['if-none-match']
                        if inm == etag:
                            return Response(error=304, page='Not modified etag')
                    else:
                        response.etag = etag
                    # refresh the session time
                    # after handling 304 responses
                    # otherwise landing on an autorefresh page would never expire
                    # and not on static or redirections
                    #if self.session is not None:
                    #    self.session.update_time()
                    #    if 'sessionid' in self.cookies:
                    #        self.cookies['sessionid']['expires'] = (
                    #                time.ctime(self.session.time + 8 * 3600) # expire in 8 hours
                    #        )
                    self.logger.info('template = %s; params = %s',
                            response.template, sanitize(params))
                    page = template.render(params)
                    response.page = page.rstrip('\n\r') + '\r\n'
            except Exception:
                self.logger.exception('Exception')
                t, e, tb = sys.exc_info()  # thread specific
                template = self.get_template('exception')
                params = {
                    'lines': traceback.format_exception(t, e, tb),
                    'username': self.session and
                        (self.session.sudo or self.session.user) or '',
                    'release': self.session and self.session.release or '',
                    'root': self.root_redirect,
                }
                if self.session and self.session.sudo:
                    params['sudo'] = True  # undefined otherwise
                page = template.render(params)
                self.send_response(500)
                if len(self.cookies) > 0:
                    self.send_header('Set-Cookie', self.cookies.output(header=''))
                self.send_header('Content-Type', 'text/html')
                #self.send_header('Connection', 'close')
                self.send_header('Content-Length', str(len(page)))
                self.end_headers()
                self.wfile.write(page.encode(self.encoding))
                return None
            else:
                self.logger.debug('process returning %s', response)
                return response

    def reply_error(self, response):
        self.logger.info('reply_error(%s)', response)
        if response.error == 405:
            msg = (
                'This page cannot be accessed using %s method'
                    % self.command)
            self.send_response(response.error)
            self.send_header('Allow', 'POST')
            if self.referer:
                self.send_header('Referer', self.referer)
            if len(self.cookies) > 0:
                self.send_header('Set-Cookie', self.cookies.output(header=''))
            self.send_header('Connection', self.want_keep_alive)
            self.send_header('Content-type', 'text/plain')
            self.send_header('Content-length', str(len(msg)))
            self.end_headers()
            if self.command != 'HEAD':
                self.wfile.write(msg)
        else:
            self.send_error(response.error, response.page)

    def reply_redirect(self, response):
        self.send_response(302)
        self.send_header('Location', response.redirect)
        self.send_header('Connection', self.want_keep_alive)
        self.send_header('Content-Length', '0')
        if self.referer:
            self.send_header('Referer', self.referer)
        if len(self.cookies) > 0:
            self.send_header('Set-Cookie', self.cookies.output(header=''))
        self.end_headers()

    def reply_static(self, response):
        accenc = self.headers.get('accept-encoding', None)
        wants_gzip = False  #accenc and ('gzip' in [l.strip() for l in accenc.split(',')])
        path = os.path.join(self.staticdir, response.static)
        ctype = response.ctype
        binary = (ctype.startswith('image/') or
                  (ctype.startswith('application/') and
                   not ctype.endswith('/javascript')))
        try:
            statfile = open(path, binary and 'rb' or 'rt')
        except IOError:
            self.send_error(404, 'no static file')
        else:
            if binary:
                page = statfile.read()
            else:
                page = statfile.read()
                if hasattr(page, 'decode'):
                    page = page.decode(self.encoding)
            statfile.close()
            self.logger.debug('path = %s, pagelen = %d', path, len(page))
            etag = gen_etag(path, {})
            self.send_response(200)
            self.send_header('Connection', self.want_keep_alive)
            if binary:
                self.send_header('Content-Type', ctype)
            else:
                self.send_header('Content-Type', '%s; charset=%s' %
                            (ctype, self.encoding))
            if wants_gzip:
                page = zlib.compress(page.encode(self.encoding))
                self.logger.debug('gzip of page, pagelen = %d', len(page))
                self.send_header('Content-Encoding', 'gzip')
            else:
                page = page.encode(self.encoding)
                if self.server_type != 'cgi':
                    self.send_header('Content-Encoding', 'identity')
            self.send_header('Cache-Control',
                    'no-transform,public,max-age=300,s-maxage=900')
            self.send_header('Content-Length', str(len(page)))
            self.send_header('Last-Modified', time.ctime(os.path.getmtime(path)))
            self.send_header('ETag', etag)
            self.end_headers()
            if self.command != 'HEAD':
                self.wfile.write(page)

    def reply_page(self, response):
        self.logger.debug(repr(response))
        accenc = self.headers.get('accept-encoding', None)
        wants_gzip = False  # accenc and ('gzip' in [l.strip() for l
        self.send_response(200)
        self.send_header('Connection', self.want_keep_alive)
        if len(self.cookies) > 0:
            self.send_header('Set-Cookie', self.cookies.output(header=''))
        if response.ctype:
            self.send_header('Content-Type', response.ctype)
        else:
            self.send_header('Content-Type', 'text/html')
        page = response.page
        if wants_gzip:
            page = zlib.compress(page.encode(self.encoding))
            self.send_header('Content-Encoding', 'gzip')
        else:
            page = page.encode(self.encoding)
            if self.server_type != 'cgi':
                self.send_header('Content-Encoding', 'identity')
        if response.etag:
            self.send_header('Etag', response.etag)
        self.send_header('Content-Length', str(len(page)))
        self.end_headers()
        if self.command != 'HEAD':
            self.wfile.write(page)

    def get_template(self, name):
        fname = '%s.tmpl' % name
        if self.tenv is None:
            loader = FileSystemLoader(self.templatedir)
            self.tenv = Environment(loader=loader, auto_reload=True)
            self.tenv.tests['startswith']= lambda s, p: s.startswith(p)
        return self.tenv.get_template(fname)

    def add_oplog(self, db, operation):
        """Add an entry to the oplog table, including the session
release and username.  We use username for historical context.  Usernames
could change later."""
        # assume that the db is already in the context of a transaction
        # if we explicitly add another level of transaction here, we may
        # end up with db locks
        db.execute(
            'insert into oplog (release, user, op) values (?, ?, ?)',
            (self.session.release, self.session.user, operation)
        )

    def get_clientip(self):
        # if coming through an apache httpd proxy connection
        client = self.client_address[0]
        if 'X-Forwarded-For' in self.headers:
            clientip = str(self.headers['X-Forwarded-For'])
            self.logger.debug('X-Forwarded-For: %s', clientip)
        else:
            clientip = socket.gethostbyname(client)
        return client, clientip

    def invalidate_cookie(self, cookie):
        """Invalidate a cookie on the client side."""
        self.cookies[cookie].value = ''
        self.cookies[cookie]['expires'] = time.ctime(0)  # set expires to posix epoch

    def valid_session_data(self):
        """Check if the user and release values in the session are
(still) valid in the database."""
        if self.session is None:
            return
        c = self.db.cursor()
        if self.session.user is not None:
            c.execute('select id from user where name = ?',
                    (self.session.user,))
            f = c.fetchone()
            if f is None:
                raise ValueError('user')
        if self.session.release is not None:
            c.execute('select name from release where name = ?',
                    (self.session.release,))
            f = c.fetchone()
            if f is None:
                raise ValueError('release')

    statelabel = {
        'notstarted': 'Not started',
        'testing': 'Testing',
        'deploying': 'Deploying',
        'verified': 'Verified'
    }
    statelabels = ('notstarted', 'deploying', 'testing', 'verified')
    @staticmethod
    def stateclass(state, passfail):
        """Return the css class name corresponding to the state and
passfail values.  Right now, there is a direct relationship from
state to class name, but that might change, so do not just return
the state."""
        if passfail == 'fail':
            return 'failure'
        elif state == 'verified':
            return 'verified'
        elif state == 'notstarted':
            return 'notstarted'
        elif state == 'deploying':
            return 'deploying'
        elif passfail == 'done':
            return 'done'
        elif state == 'testing':
            return 'testing'
        else:
            return ''

    @staticmethod
    def is_disabled(state):
        if state in ('notstarted', 'deploying'):
            return 'disabled'
        else:
            return ''

    @staticmethod
    def valid_name(name):
        '''valid name are /[a-zA-Z][a-zA-Z0-9_.-]*/'''
        from string import lowercase, uppercase, digits
        valid = lowercase + uppercase + digits + '_-.'
        if len(name) < 1:
            return False
        if name[0] not in lowercase + uppercase:
            return False
        for c in name[1:]:
            if c not in valid:
                return False
        else:
            return True

    def get_perms(self, real=False):
        perms = set()
        roles = set()
        if self.session is None:
            return None, None
        if self.session.user is not None:
            if real:
                user = self.session.user
            else:
                user = self.session.sudo or self.session.user
            cur = self.db.cursor()
            s = cur.execute(
                    'select perm.permid, role.name from perm, user, role '
                    'where perm.role = role.name and user.name = ? and '
                    '(role.user = user.name or role.user = user.id)',
                        (user,)).fetchall()
            perms.update(set(l[0] for l in s))
            roles.update(set(l[1] for l in s))
        self.logger.info('user = %s/%s; roles = %s; permissions = %s',
                self.session.user, self.session.sudo or '----',
                sorted(roles), sorted(perms))
        return roles, perms

    def change_password(self, uid, passwd):
        """Assign a new salted, encoded password to a user, deleting any
password already assigned."""
        with self.db as db:
            # see if there is an existing password assigned, save it
            # to delete at the end of the transaction
            existing_pid = db.execute(
                    'select password from user where id = ?', (uid,)
            ).fetchone()
            # if there is one, get the valid (first item in sequence)
            if existing_pid:
                existing_pid = existing_pid[0]
            c = db.execute(
                    'insert into password (salt1, salt2, password) '
                    'values (?, ?, ?)', genpassword(passwd)
            )
            new_pid = c.lastrowid
            db.execute(
                    'update user set password = ? where id = ?',
                    (new_pid, uid))
            if existing_pid:
                db.execute('delete from password where id = ?',
                    (existing_pid,))

    def get_statistics(self):
        """Scan through the testcases for this release and determine the total
to be completed, how many pass, how many failed and break it down by user and
by product."""
        cur = self.db.cursor()
        s = (
            'select testcase.tid, user.name, testcase.product, '
            'release.state, passfail.status from release inner '
            'join testcase on release.name = ? and release.product = '
            'testcase.product inner join assignment on release.name = '
            'assignment.release and testcase.tid = assignment.test '
            'inner join user on assignment.user = user.id left outer '
            'join passfail on testcase.tid = passfail.tid and '
            'release.name = passfail.release;')
        tot = {'ytd': 0, 'pass': 0, 'fail': 0}
        users, products = {}, {}
        for (tid, user, product, state, status) in cur.execute(s, (self.session.release,)):
            if user in users:
                u = users[user]
            else:
                u = users[user] = {
                    'class': '',
                    'name': user, 'ytd': 0, 'pass': 0, 'fail': 0}
            if product in products:
                p = products[product]
            else:
                p = products[product] = {
                    'class': '', 'label': self.statelabel[state],
                    'name': product, 'state': state, 'ytd': 0, 'pass': 0,
                    'fail': 0}
            if status is None:
                mode = 'ytd'
            else:
                mode = status
            tot[mode] += 1
            u[mode] += 1
            p[mode] += 1
        attrs = {'totals': tot, 'users': users, 'products': products}
        return attrs

    # pre-processors for pages
    # page handlers all start with "call_"
    # data structures used:
    #   db
    #   logger
    #   config
    #   query
    #   session
    #   perms
    #   referer
    #   staticdir
    #   templatedir
    #   server_type
    #   server

    def call_about(self):
        import jinja2, sqlite3
        attrs = {
            'vers': {
                'inquisitor': version,
                'jinja': jinja2.__version__,
                'sqlite': sqlite3.version,
                'python': sys.version,
            }
        }
        return Response('about', attrs)

    def call_admin(self):
        if 'ADMIN' not in self.perms:
            self.logger.error('no access to admin; redirect back to %s',
                    self.referer or 'main')
            self.session.error = 'Permission denied'
            return Response(redirect=self.referer or 'main')
        return Response('admin')

    def call_admin_activations(self):
        if 'ADMIN' not in self.perms or 'RELEASE' not in self.perms:
            self.logger.error('no access to admin')
            self.session.error = 'Permission denied'
            return Response(redirect=('ADMIN' in self.perms and 'admin' or 'main'))
        cur = self.db.cursor()
        sql1 = 'select distinct product from testcase'  # get all products
        sql2 = 'select product from release where name = ?'  # which are active
        products = [l[0] for l in cur.execute(sql1)]
        submit = self.query.getfirst('submit')
        if submit is not None:
            self.logger.info('modify activations')
            if 'new_release' in self.query:
                currelease = self.query.getfirst('new_release')
            elif 'release' in self.query:
                currelease = self.query.getfirst('release')
            if currelease == '--Release--':  # sentinal == None
                currelease = None
            if currelease is None:  # if it is still None, then error
                self.logger.error('Expecting a valid release.')
                self.session.error = 'Expecting a release, None given'
            else:
                formproducts = self.query.getlist('product')
                activeproducts = [l[0] for l in cur.execute(sql2, (currelease,))]
                with self.db:
                    for product in products:
                        try:
                            if ((product in formproducts and product in activeproducts) or
                                (product not in formproducts and product not in activeproducts)):
                                self.logger.debug('ignoring %s', product)
                                pass  # no change
                            elif (product in formproducts and product not in activeproducts):
                                op = 'inserting'
                                self.logger.info('activating %s', product)
                                activeproducts.append(product)
                                self.db.execute(
                                    "insert into release (name, product) values (?, ?)",
                                    (currelease, product)
                                )
                            elif (product not in formproducts and product in activeproducts):
                                op = 'deleting'
                                self.logger.info('deactivating %s', product)
                                activeproducts.remove(product)
                                self.db.execute(
                                    "delete from release where name = ? and product = ?",
                                    (currelease, product)
                                )
                        except sqlite3.IntegrityError:
                            # already in the db?
                            self.logger.error('IntegrityError: %s %s %s', op, product, currelease)
                            self.session.error = 'Duplicate entry'
        elif 'currelease' in self.query:
            currelease = self.query.getfirst('currelease')
        else:
            currelease = self.session.release
        activeproducts = [l[0] for l in cur.execute(sql2, (currelease,))]
        self.logger.info('activeproducts %s on %s', activeproducts, currelease)
        options = []
        fields = []
        options = [r[0] for r in cur.execute('select distinct name from release')]
        for product in products:
            fields.append(
                {'product': product,
                     'checked': product in activeproducts and 'checked' or ''})
        attrs = {'currelease': currelease, 'rows': fields, 'options': options }
        return Response('admin_activations', attrs)

    def call_admin_assignments(self):
        sql = ('select test, user.name from user, assignment '
               'where release = ? and user.id = assignment.user')
        if 'ADMIN' not in self.perms or 'ASSIGN' not in self.perms:
            self.logger.error('no access to admin_assignments')
            self.session.error = 'Permission denied'
            return Response(redirect=('ADMIN' in self.perms and 'admin' or 'main'))
        selectuser = self.query.getfirst('selectuser', 'Tester')
        selectprod = self.query.getfirst('selectprod', 'Product')
        submit = self.query.getfirst('do_submit')
        cur = self.db.cursor()
        if submit:
            self.logger.info('modify assignment(s)')
            if submit.lower() == 'dup':
                release_to_copy = self.query.getfirst('rel2dup')
                # duplicate the assignments from another release, but only
                # for those products that are active in this release
                s = (
    'insert or replace into assignment select a.user, a.test, ?'
    'from assignment as a, testcase as t, release as r '
    'where a.test = t.tid and a.release = r.name and t.product = r.product '
    'and r.name == ? and t.product in (select product from release where name = ?)')
                with self.db as db:
                    data = (self.session.release, release_to_copy, self.session.release)
                    db.execute(s, data)
            else:
                assignments = dict(cur.execute(sql, (self.session.release,)).fetchall())
                with self.db:
                    for var in self.query:
                        if var.startswith('tester_'):
                            val = self.query.getfirst(var)
                            tid = int(var[7:])
                            self.logger.debug('var = %s; val = %s; tid = %s, assign = %s',
                                    var, val, tid, assignments.get(tid, '(none)'))
                            if ((val == '-' and tid not in assignments) or
                                (tid in assignments and val == assignments[tid])):
                                continue
                            elif val == '-':
                                self.db.execute(
                'delete from assignment where test = ? and release = ?',
                                    (tid, self.session.release))
                            elif tid not in assignments:
                                self.db.execute(
                'insert into assignment values (?, ?, ?)',
                                    (val, tid, self.session.release))
                            else:
                                self.db.execute(
                'update assignment set user = ? where test = ? and release = ?',
                                    (val, tid, self.session.release))
        # get the userid and username who have the PASSFAIL perm (tester role)
        testersql = ('select user.id, user.name from user, role, perm '
                     'where user.id = role.user and role.name = perm.role and '
                     'perm.permid = "PASSFAIL"')
        testers = cur.execute(testersql).fetchall()
        # get the userid and username who have been assigned to testcases
        # in the current release who also have the PASSFAIL perm
        usql = ('select distinct user.id, user.name from user, role, perm, '
                'assignment as a, release as r, testcase as t '
                'where user.id = role.user and role.name = perm.role and '
                'perm.permid = "PASSFAIL" and user.id = a.user and r.name = ? '
                'and r.name = a.release and a.test = t.tid and '
                'r.name = a.release and r.product = t.product')
        userlist = cur.execute(usql, (self.session.release,)).fetchall()
        userlist.insert(0, ('-', '----'))
        usermap = dict((u[1], u[0]) for u in userlist)
        assignments = dict(cur.execute(sql, (self.session.release,)).fetchall())
        releases = [r[0] for r in cur.execute('select distinct name from release')]
        try:
            releases.remove(self.session.release)
        except ValueError:
            pass
        fields = []
        count = 0
        # accumulate the list of testcase for the current release
        # order by product then testcase name
        # allow for limiting the view by tester or product or both
        # selectuser == 'Tester' == any user
        # selectuser == '----'  == unassigned
        # selectprod == 'Product' == any product
        if selectuser == '----' and selectprod != 'Product':
            sql = ('select t.* from testcase as t, release as r, '
                   'user as u where r.name = ? and r.product = ? and '
                   'r.product = t.product and not exists '
                   '(select * from assignment as a where t.tid = a.test and '
                   'a.release = r.name) group by t.tid order by t.product, t.name')
            data = (self.session.release, selectprod)
        elif selectuser != 'Tester' and selectprod != 'Product':
            sql = ('select t.* from testcase as t, release as r, assignment as a, '
                   'user as u where u.name = ? and r.name = ? and '
                   't.product = ? and r.product = t.product and a.user = u.id and '
                   'a.release = r.name and a.test = t.tid order by t.product, t.name')
            data = (selectuser, self.session.release, selectprod)
        elif selectuser == '----':
            sql = ('select t.tid, t.product, t.name from testcase as t, '
                   'release as r where r.name = ? and r.product = t.product and '
                   't.tid not in (select a.test from assignment as a where '
                   'a.release = r.name) group by t.tid order by t.product, t.name')
            data = (self.session.release,)
        elif selectuser != 'Tester':
            sql = ('select t.tid, t.product, t.name from testcase as t, '
                   'release as r, assignment as a, user as u '
                   'where a.user = u.id and r.name = ? and u.name = ? and '
                   'a.test = t.tid and t.product = r.product and '
                   'a.release = r.name group by t.tid '
                   'order by t.product, t.name')
            data = (self.session.release, selectuser)
        elif selectprod != 'Product':
            sql = ('select t.tid, t.product, t.name from testcase as t, '
                   'release as r where r.name = ? and '
                   'r.product = ? and t.product = r.product '
                   'group by t.tid order by t.product, t.name')
            data = (self.session.release, selectprod)
        else:
            sql = ('select t.tid, t.product, t.name from testcase as t, '
                   'release where release.name = ? and '
                   't.product = release.product '
                   'order by t.product, t.name')
            data = (self.session.release,)
        self.logger.debug('sql = %s; data = %s', sql, data)
        for entry in cur.execute(sql, data):
            tid = entry[0]
            attrs = {'tid': tid, 'product': entry[1], 'name': entry[2],
                     'selected': ''}
            if tid not in assignments:
                attrs['selected'] = '-'
                count += 1
            elif tid in assignments:
                attrs['selected'] = usermap[assignments[tid]]
            fields.append(attrs)
        fields.sort(key=lambda x: (x['product'], x['name']))
        userlist = [{'uid': u[0], 'name': u[1]} for u in userlist]
        testers = [{'uid': u[0], 'name': u[1]} for u in testers]
        testers.insert(0, {'uid': '-', 'name': '----'})
        prodlist = [p[0] for p in cur.execute('select product from release where name = ?',
                                              (self.session.release,))]
        return Response('admin_assignments',
                {'rows': fields, 'count': count, 'releases': releases,
                    'users': userlist, 'products': prodlist, 'testers': testers,
                 'selectuser': selectuser, 'selectprod': selectprod})

    def call_admin_info(self):
        if 'ADMIN' not in self.perms:
            self.logger.error('no accesss to admin_info; redirect back to %s', self.referer)
            self.session.error = 'Permission denied'
            return Response(redirect=self.referer or 'main')
        cur = self.db.cursor()
        dbversions = []
        try:
            cur.execute('select name, applied from version order by applied desc')
            for vers in cur:
                dbversions.append({
                    'vers': vers[0],
                    'applied': vers[1]
                })
        except:
            lastapplied = 'db error'
            numusers = 'unknown'
        numusers = 0
        sessions = []
        for f in cur.execute('select user, time, id from session'):
            data = {'name': f[0], 'time': time.ctime(f[1]), 'sessionid': f[2]}
            if data['sessionid'] == self.session.id:
                data['name'] = '&lt;me&gt;'
            if data['name'] is None:
                data['name'] = '&lt;none&gt;'
            sessions.append(data)
            if f[0]:
                numusers += 1
        staticfiles = sorted(os.listdir(self.staticdir))
        templatefiles = []
        for dn, ds, fs in os.walk(self.templatedir):
            for n in sorted(fs):
                fn = os.path.join(dn, n)
                fn = os.path.relpath(fn, self.templatedir)
                templatefiles.append(
                    os.path.relpath(os.path.join(dn, n), self.templatedir)
                )
            ds.sort()
        #templatefiles = sorted(os.listdir(self.templatedir))
        tables = []
        for tname in constants.TableNames:
            query = 'select count(*) from %s' % tname
            cur.execute(query)
            total = cur.fetchone()[0]
            if tname in constants.TableNamesWRelease and self.session.release:
                if tname == 'release':
                    query += ' where name = ?'
                else:
                    query += ' where release = ?'
                cur.execute(query, (self.session.release,))
                relcount = cur.fetchone()[0]
            else:
                relcount = None
            tables.append({'name': tname, 'count': total, 'release': relcount})
        starttime = (self.server_type == 'httpd' and
                     time.ctime(self.server.starttime) or
                     None)
        def uptime(stime):
            dur_sec = time.time() - stime
            m, s = divmod(dur_sec, 60)
            h, m = divmod(m, 60)
            return '%6d:%02d:%02.1f' % (h, m, s)

        attrs = {
            'config': self.config,
            'sessions': sessions,
            'dbvers': dbversions,
            'numusers': numusers,
            'starttime': starttime,
            'staticfiles': staticfiles,
            'tables': tables,
            'templatefiles': templatefiles,
            'uptime': uptime(self.server.starttime),
        }
        return Response('admin_info', attrs)

    def call_admin_master(self):
        if 'ADMIN' not in self.perms or 'MASTER' not in self.perms:
            self.logger.error('no access to admin_master')
            self.session.error = 'Permission denied'
            return Response(redirect=('ADMIN' in self.perms and 'admin' or 'main'))
        selectprod = self.query.getfirst('selectprod', 'Product')
        tid = self.query.getfirst('tid', None)
        op = self.query.getfirst('op', None)
        if 'do_submit' in self.query:
            submit = self.query.getfirst('do_submit').lower()
        else:
            submit = None
        goto = self.query.getfirst('do_goto', None)
        s_term = self.query.getfirst('do_search', None)
        s_prod = self.query.getfirst('do_prod', None)
        s_info = self.query.getfirst('do_info', None)
        s_name = self.query.getfirst('do_name', None)
        self.logger.debug('admin_master: tid= %s, op=%s, submit=%s', tid, op, submit)
        if submit in ('back', 'cancel'):
            return Response(redirect='admin_master')
        elif op == 'add':
            try:
                name = self.query.getfirst('name')
                product = self.query.getfirst('product')
                info = self.query.getfirst('info')
                assert name is not None
                assert product is not None
                assert info is not None
            except AssertionError:
                self.logger.error('Missing fields for admin_master[add]')
                self.session.error = 'Data missing'
                return Response(redirect='admin_master?do_submit=New')
            else:
                try:
                    with self.db:
                        self.db.execute(
                            "insert into testcase " + \
                            "('name', 'product', 'info') " + \
                            "values (?, ?, ?)",
                            (name, product, info))
                        self.db.execute(
                            'select tid from testcase where name = ? and product = ?',
                            (name, product))
                        cur = self.db.cursor()
                        rows = cur.fetchall()
                        if len(rows) != 1:
                            self.logger.error('more than one testcase (%s, %s)', name, product)
                            return Response(redirect='admin_master')
                        else:
                            tid = rows[0][0]
                except sqlite3.IntegrityError:
                    self.logger.error('Error adding testcase: duplicate entry')
                    self.session.error = 'Duplicate entry'
                self.logger.info('save new tid %s', tid)
            return Response(redirect='admin_master')
        elif op == 'edit':
            try:
                tid = self.query.getfirst('tid')
                name = self.query.getfirst('name')
                product = self.query.getfirst('product')
                info = self.query.getfirst('info')
                assert tid is not None
                assert name is not None
                assert product is not None
                assert info is not None
            except AssertionError:
                self.logger.error('Missing fields for admin_master[edit]')
                return Response(redirect='admin_master')
            else:
                with self.db:
                    self.db.execute(
                        'update testcase set name = ?, product = ?, info = ?' + \
                            'where tid = ?',
                        (name, product, info, tid)
                    )
            self.logger.info('save tid %s', tid)
            return Response(redirect='admin_master')
        elif op == 'delete':
            if submit == 'yes':
                try:
                    tid = self.query.getfirst('tid')
                    assert tid is not None
                except AssertionError:
                    self.logger.error('Missing fields for admin_master[delete]')
                    self.session.error = 'Data missing'
                else:
                    with self.db:
                        self.db.execute(
                            'delete from testcase where tid = ?',
                            (tid,)
                        )
                    self.logger.info('delete tid %s', tid)
            return Response(redirect='admin_master')
        elif submit == 'new':
            return Response('admin_master_add')
        elif submit in ('delete', 'dup', 'edit'):
            cur = self.db.cursor()
            cur.execute(
                'select name, product, info from testcase where tid = ?',
                (tid,))
            f = cur.fetchone()
            if f is not None:
                if submit == 'dup':
                    template = 'admin_master_add'
                else:
                    template = 'admin_master_%s' % submit
                return Response(template, {
                    'tid': tid, 'name': f[0], 'product': f[1], 'info': f[2]})
            else:
                return Response(redirect='admin_master')
        elif submit == 'search' and goto is not None:
            cur = self.db.cursor()
            cur.execute('select * from testcase where tid = ?', (goto,))
            try:
                tid, name, product, info = cur.fetchone()
            except:
                self.session.error = 'Invalid test case id'
                return Response(redirect='admin_master')
            attrs = {
                'tid': tid,
                'name': name,
                'product': product,
                'info': info,
            }
            return Response('admin_master_show', attrs)
        else:
            cur = self.db.cursor()
            fields = []
            products = set()
            if selectprod == 'Product':
                sql = 'select tid, product, name, info from testcase'
                data = ()
            else:
                sql = 'select tid, product, name, info from testcase where product = ?'
                data = (selectprod,)
            products = [f[0] for f in cur.execute('select distinct product from testcase')]
            # generate a search function
            if submit == 'search':
                def dosearch(d, t=s_term, n=s_name, i=s_info, p=s_prod):
                    prod = d[1].lower()
                    name = d[2].lower()
                    info = d[3].lower()
                    return (
                        (not t or ('%s%s%s' % (prod, name, info)).find(t) != -1) and
                        (not p or prod.find(p) != -1) and
                        (not n or name.find(n) != -1) and
                        (not i or info.find(i) != -1))
                self.logger.debug('searching against %s or %s or %s', s_term, s_name, s_info)
            else:
                dosearch = lambda f: True
            fields = [
                {'tid': f[0], 'product': f[1], 'name': f[2]}
                    for f in cur.execute(sql, data)
                        if dosearch(f)
            ]
            search = {
                'info': s_info or '', 'name': s_name or '',
                'prod': s_prod or '', 'term': s_term or '',
            }
            fields.sort(key=lambda x: (x['product'], x['name']))
            return Response('admin_master',
                    {'rows': fields, "products": products,
                     'selectprod': selectprod, 'search': search})

    def call_admin_prodinfo(self):
        if 'ADMIN' not in self.perms:
            self.logger.error('no access to admin_prodinfo')
            self.session.error = 'Permission denied'
            return Response(redirect='main')
        cur = self.db.cursor()
        submit = self.query.getfirst('do_submit', None)
        op = self.query.getfirst('op', None)
        selectprod = self.query.getfirst('selectprod', None)
        products = [f[0] for f in
                cur.execute('select distinct product from testcase')]
        def dtype(data):
            if '@' in data:
                return 'userhost'
            elif (data.startswith('http://') or
                  data.startswith('https://')):
                return 'url'
            else:
                raise ValueError('Invalid data type')
        if submit and submit.lower() == 'cancel':
            template = 'admin_prodinfo'
        elif (op and op.lower() == 'load' and
                submit and submit.lower() == 'save'):
            data = self.query.getfirst('data')
            fields = [f.split() for f in data.strip().split('\n')]
            toadd = []
            data = [
                (f[0], dtype(f[1]), f[1]) for f in fields
                    if len(f) == 2 and f[0] in products
            ]
            sql = 'insert into productinfo values (?, ?, ?)'
            with self.db as db:
                db.executemany(sql, data)
            return Response(redirect='admin_prodinfo')
        elif (op and op.lower() == 'add' and
                submit and submit.lower() == 'save'):
            product = self.query.getfirst('product')
            if product is None:
                self.session.error = 'Data missing'
                return Response(redirect='admin_prodinfo')
            try:
                data = [
                    (product, dtype(d), d) for d in
                        [f.strip() for f in self.query.getlist('data')]
                        if d
                ]
            except ValueError:
                t, e, tb = sys.exc_info()
                self.session.error = e.args[0]
                return Response(redirect='admin_prodinfo')
            sql = 'insert into productinfo values (?, ?, ?)'
            with self.db as db:
                db.executemany(sql, data)
            return Response(redirect='admin_prodinfo')
        elif (op and op.lower() == 'edit' and
                submit and submit.lower() == 'save'):
            with self.db as db:
                for lindex in [f[3:] for f in self.query if f.startswith('id_')]:
                    product = self.query.getfirst('prod_%s' % lindex)
                    rowid = self.query.getfirst('id_%s' % lindex)
                    data = self.query.getfirst('data_%s' % lindex)
                    db.execute(
                        'update or ignore productinfo set product = ?, '
                        'data = ?, type = ? where rowid = ?',
                        (product, data, dtype(data), rowid))
            return Response(redirect='admin_prodinfo')
        elif (op and op.lower() == 'delete' and
                submit and submit.lower() == 'save'):
            rowids = self.query.getlist('rowid')
            with self.db as db:
                for i in rowids:
                    db.execute('delete from productinfo where rowid = ?',
                        (i,))
            return Response(redirect='admin_prodinfo')
        elif submit and submit.lower() == 'edit':
            template = 'admin_prodinfo_edit'
        elif submit and submit.lower() == 'new':
            template = 'admin_prodinfo_add'
        elif submit and submit.lower() == 'load':
            template = 'admin_prodinfo_load'
        elif submit and submit.lower() == 'delete':
            template = 'admin_prodinfo_delete'
        else:
            template = 'admin_prodinfo'
        if selectprod and selectprod != 'Product':
            sql = ('select rowid, product, type, data from productinfo '
                   'where product = ?')
            data = (selectprod,)
        else:
            sql = ('select rowid, product, type, data from productinfo')
            data = ()
        fields = []
        for field in cur.execute(sql, data):
            fields.append(
                {'rowid': field[0], 'product': field[1],
                 'type': field[2], 'data': field[3]})
        fields.sort(key=lambda f: (f['product'], f['type'], f['data']))
        return Response(template, {
                    'products': products,
                    'selectprod': selectprod or 'Product',
                    'fields': fields})

    def call_admin_role(self):
        if 'ADMIN' not in self.perms or 'ROLES' not in self.perms:
            self.logger.error('no access to admin_role')
            self.session.error = 'Permission denied'
            return Response(redirect=('ADMIN' in self.perms and 'admin' or 'main'))
        cur = self.db.cursor()
        op = self.query.getfirst('op')
        submit = self.query.getfirst('submit')
        if op == 'roles' and submit == 'New':
            pass
        elif op == 'roles' and submit == 'Save':
            if self.command == 'GET':
                return Response(error=405)
            newrole = self.query.getfirst('newrole', '-new-')
            roleperms = {}
            for key in self.query:
                if not key.startswith('perm_'):
                    continue
                role = key[5:]
                roleperms[role] = self.query.getlist(key)
            roles = [l[0] for l in cur.execute('select distinct role from perm')]
            if newrole != '-new-':
                if not self.valid_name(newrole):
                    self.session.error = 'Invalid name: %s' % newrole
                    return Response(redirect='admin_role')
                try:
                    roleperms[newrole] = roleperms['-new-']
                except KeyError:
                    self.session.error = 'Must have some permissions'
                    return Response(redirect='admin_role')
            # even if we have a new one, that was copied to the new name, delete this
            if '-new-' in roleperms:
                del roleperms['-new-']
            toadd = []
            torem = []
            for name in roles:
                storedperms = [l[0] for l in
                    cur.execute('select permid from perm where role = ?', (name,))]
                if name not in roleperms:
                    for sp in storedperms:
                        torem.append( (sp, name) )
                else:
                    for sp in storedperms:
                        if sp not in roleperms[name]:
                            torem.append( (sp, name) )
                        else:
                            roleperms[name].remove(sp)
                    if name in roleperms:
                        for rp in roleperms[name]:
                            if rp not in storedperms:
                                toadd.append( (rp, name) )
                    del roleperms[name]
            if newrole != '-new-':
                for rp in roleperms[newrole]:
                    toadd.append( (rp, newrole) )
            self.logger.info('admin_role[roles] toadd = %s, torem = %s', toadd, torem)
            with self.db as db:
                if toadd:
                    db.executemany('insert or ignore into perm '
                        '(permid, role) values (?, ?)', toadd)
                if torem:
                    db.executemany('delete from perm where permid = ? '
                        'and role = ?', torem)
        elif op == 'users' and submit == 'Save':
            if self.command == 'GET':
                return Response(error=405)
            userroles = {}
            for key in self.query:
                if not key.startswith('role_'):
                    continue
                uid = int(key[5:])
                userroles[uid] = self.query.getlist(key)
            users = dict(cur.execute('select name, id from user'))
            toadd = []
            torem = []
            for name in users:
                uid = users[name]
                storedroles = [l[0] for l in
                    cur.execute('select name from role where user = ?', (uid,))]
                if uid not in userroles:
                    for sr in storedroles:
                        torem.append( (sr, uid) )
                else:
                    for sr in storedroles:
                        if sr not in userroles[uid]:
                            torem.append( (sr, uid) )
                        else:
                            userroles[uid].remove(sr)
                    if uid in userroles:
                        for ur in userroles[uid]:
                            if ur not in storedroles:
                                toadd.append( (ur, uid) )
            self.logger.debug('admin_role[users] toadd = %s, torem = %s', toadd, torem)
            with self.db as db:
                if toadd:
                    db.executemany('insert into role (name, user) values (?, ?)', toadd)
                if torem:
                    db.executemany('delete from role where name = ? and user = ?', torem)
        roles = set()
        perms = set()
        roleperms = {}
        userroles = {}
        for (r, p) in cur.execute('select * from perm'):
            roles.add(r)
            perms.add(p)
            if r in roleperms:
                roleperms[r].append(p)
            else:
                roleperms[r] = [p]
        cur.execute('select user, name from role')
        for (u, r) in cur:
            if u in userroles:
                userroles[u].append(r)
            else:
                userroles[u] = [r]
        users = list(cur.execute('select name, id from user'))
        return Response('admin_role',
            {'roles': roles, 'perms': constants.AllPerms, 'users': users,
             'usersroles': userroles, 'roleperms': roleperms,
            }
        )

    def call_admin_sessions(self):
        if 'ADMIN' not in self.perms or 'USERS' not in self.perms:
            self.logger.error('no access to admin sessions')
            self.session.error = 'Permission denied'
            return Response(redirect=('ADMIN' in self.perms and 'admin' or 'main'))
        sid = self.query.getfirst('sid')
        submit = self.query.getfirst('do_submit')
        sessions = self.query.getlist('session')
        if submit == 'Delete':
            with self.db as db:
                for session in sessions:
                    if session != self.session.id:
                        db.execute('delete from session where id = ?', (session,))
            return Response(redirect='admin_sessions')
        elif sid:
            cur = self.db.cursor()
            cur.execute(
                'select id, time, ipaddr, user, release, error, sudo '
                'from session where id = ?', (sid,))
            f = cur.fetchone()
            if f:
                attrs = {
                    'id': f[0],
                    'time': time.ctime(f[1]),
                    'ipaddr': f[2],
                    'user': f[3],
                    'release': f[4],
                    'error': f[5],
                    'sudo': f[6]
                }
                return Response('admin_session', {'session': attrs})
            else:
                self.session.error = 'No such session id'
                return Response(redirect='admin_sessions')
        else:
            cur = self.db.cursor()
            fields = [
                {'id': s[0], 'release': s[1], 'user': s[2], 'when': time.ctime(s[3])}
                    for s in cur.execute('select id, release, user, time from session')
            ]
            return Response('admin_sessions',
                {'fields': fields, 'current': self.session.id}
            )

    def call_admin_sudo(self):
        # get the user's real permissions
        roles, perms = self.get_perms(real=True)
        if 'ADMIN' not in perms or 'SUDO' not in perms:
            self.logger.error('no access to admin sudo')
            self.session.error = 'Permission denied'
            return Response(redirect=('ADMIN' in perms and 'admin' or 'main'))
        submit = self.query.getfirst('do_submit')
        user = self.query.getfirst('selectuser')
        cur = self.db.cursor()
        userlist = [u[0] for u in cur.execute('select name from user')]
        if user == '----' or user == self.session.user:
            self.session.sudo = None  # clear sudo
            self.roles, self.perms = self.get_perms()  # update with new perms
        elif user in userlist:
            self.session.sudo = user
            self.roles, self.perms = self.get_perms()  # update with new perms
        elif user is not None:
            self.session.error = 'Invalid username'
        attrs = {
            'real': self.session.user,
            'user': self.session.sudo,
            'users': userlist,
        }
        return Response('admin_sudo', attrs)

    def call_admin_tools(self):
        if 'ADMIN' not in self.perms or 'DATA' not in self.perms:
            self.logger.error('no access to admin_tools')
            self.session.error = 'Permission denied'
            return Response(redirect=('ADMIN' in self.perms and 'admin' or 'main'))
        submit = self.query.getfirst('do_submit')
        op = self.query.getfirst('op')
        params = {
            'server_type': self.server_type,
            'backup_display': 'none',
            'clearrelease_display': 'none',
            'clearforget_display': 'none',
            'reconfig_display': 'none',
            'shutdown_display': 'none',
            'restart_display': 'none',
            'releases': [
                r[0] for r in self.db.execute('select distinct name from release')
            ],
        }
        if submit and submit.lower() in ('cancel', 'no'):
            return Response(redirect='admin_tools')
        elif op == "clearrelease":
            release = self.query.getfirst('release', None)
            sentinal = '----'
            if submit == 'Continue' and release != sentinal and release is not None:
                self.logger.debug('clearrelease: gather for %s', release)
                clear = {'name': release}
                data = self.query.getlist('data')
                # if we remove from the 'release' table, then remove everyplace else
                if 'products' in data:
                    if 'assignments' not in data:
                        data.append('assignments')
                    if 'passfails' not in data:
                        data.append('passfails')
                    if 'issues' not in data:
                        data.append('issues')
                    if 'oplog' not in data:
                        data.append('oplog')
                self.logger.debug('scanning to clear for %s: %s', release, data)
                cur = self.db.cursor()
                for item in data:
                    if item == 'products':
                        sql = 'select product, state from release where name = ?'
                        clear['products'] = entries = []
                        for (p, s) in cur.execute(sql, (release,)):
                            entries.append({'name': p, 'state': s})
                    elif item == 'assignments':
                        sql = ('select u.name, t.product, t.name from user as u, testcase as t, '
                               'assignment as a where a.release = ? and a.user = u.id '
                               'and a.test = t.tid order by t.product, t.name, u.name')
                        clear['assignments'] = entries = []
                        for (u, p, t) in cur.execute(sql, (release,)):
                            entries.append({'user': u, 'product': p, 'test': t})
                        if len(entries) == 0:
                            del clear['assignments']
                    elif item == 'passfails':
                        sql = ('select t.product, t.name, p.status from passfail as p, '
                               'testcase as t where p.release = ? and p.tid = t.tid '
                               'order by t.product, t.name')
                        clear['passfails'] = entries = []
                        for (p, t, s) in cur.execute(sql, (release,)):
                            entries.append({'product': p, 'test': t, 'status': s})
                        if len(entries) == 0:
                            del clear['passfails']
                    elif item == 'issues':
                        sql = 'select name from issue where release = ? order by name'
                        clear['issues'] = entries = []
                        for i in cur.execute(sql, (release,)):
                            entries.append({'name': i[0]})
                        if len(entries) == 0:
                            del clear['issues']
                    elif item == 'oplogs':
                        sql = 'select user, op from oplog where release = ? order by time'
                        clear['oplogs'] = entries = []
                        for i in cur.execute(sql, (release,)):
                            entries.append({'user': i[0], 'op': i[1]})
                        if len(entries) == 0:
                            del clear['oplogs']
                return Response('admin_tools_clearrelease', {'clear': clear})
            elif submit == 'Yes' and release != sentinal and release is not None:
                self.logger.debug('clearrelease: deleting for %s', release)
                data = self.query.getlist('data')
                numrows = 0
                with self.db as db:
                    self.logger.info('delete %s for %s', ', '.join(data), release)
                    if 'oplogs' in data:
                        c = db.execute('delete from oplog where release = ?', (release,))
                        numrows += c.rowcount
                    if 'issues' in data:
                        c = db.execute('delete from issue where release = ?', (release,))
                        numrows += c.rowcount
                    if 'passfails' in data:
                        c = db.execute('delete from passfail where release = ?', (release,))
                        numrows += c.rowcount
                    if 'assignments' in data:
                        c = db.execute('delete from assignment where release = ?', (release,))
                        numrows += c.rowcount
                    if 'products' in data:
                        c = db.execute('delete from release where name = ?', (release,))
                        numrows += c.rowcount
                params['status'] = '%d records deleted' % numrows
            else:
                self.logger.error('clearrelease: no proper selection - submit=%s; release=%s', submit, release)
            params['clearrelease_display'] = 'block'
            return Response('admin_tools', params)
        elif op == "clearforget":
            sql = ('delete from forget where '
                   'datetime(time, "2 hours") < datetime("now")')
            with self.db as db:
                cur = db.execute(sql)
                params['status'] = '%d forgot entries removed' % cur.rowcount
            params['clearforget_display'] = 'block'
            return Response('admin_tools', params)
        elif op == 'backup':
            import glob, shutil
            dbfile = self.database_filename
            if submit == 'Backup':
                self.logger.debug('backup: copying %s', dbfile)
                backups = sorted(glob.glob('%s.[0-9][0-9]' % dbfile))
                if backups:
                    last = backups[-1]
                else:
                    last = 0
                if last:
                    ext = os.path.splitext(last)[1][1:]   # strip off the leading '.'
                    try:
                        last = int(ext, 10)
                    except ValueError:
                        last = 0
                next = last + 1
                nextfile = '%s.%02d' % (dbfile, next)
                try:
                    shutil.copy2(dbfile, nextfile)
                except OSError:
                    t, e, tb = sys.exc_info()
                    self.logger.error('Error %s: %s', e, nextfile)
                    self.session.error = 'Could not copy database file'
                else:
                    params['status'] = 'Db copied to %s' % nextfile
            elif submit == 'Cleanup':
                try:
                    age = int(self.query.getfirst('age'))
                except ValueError:
                    self.logging.warning('User input invalid: age on admin_tools/backup')
                    self.session.error = 'Invalid input for age'
                else:
                    logan = time.time() - age * 24 * 3600  # N days
                    self.logger.debug('cleanup backup files')
                    toclean = 0
                    backups = sorted(glob.glob('%s.[0-9][0-9]' % dbfile))
                    for fname in backups:
                        mtime = os.path.getmtime(fname)
                        if mtime < logan:
                            self.logger.debug('removing %s', fname)
                            os.remove(fname)
                            toclean += 1
                    if toclean > 0:
                        params['status'] = '%d files deleted' % toclean
            params['backup_display'] = 'block'
            return Response('admin_tools', params)
        elif op == 'reconfig':
            if self.server_type == 'httpd':
                self.signal_reload()
                params['status'] = 'configurations reloaded'
            params['reconfig_display'] = 'block'
            return Response('admin_tools', params)
        elif op == 'shutdown':
            if self.server_type == 'httpd':
                self.signal_shutdown()
                params['status'] = 'server being shutdown'
            params['shutdown_display'] = 'block'
            return Response('admin_tools', params)
        elif op == 'restart':
            if self.server_type == 'httpd':
                self.signal_restart()
                params['status'] = 'server being restarted'
            params['restart_display'] = 'block'
            return Response('admin_tools', params)
        else:
            return Response('admin_tools', params)

    def call_admin_user(self):
        if 'ADMIN' not in self.perms or 'USERS' not in self.perms:
            self.logger.error('no access to admin_user')
            self.session.error = 'Permission denied'
            return Response(redirect=('ADMIN' in self.perms and 'admin' or 'main'))
        uid = self.query.getfirst('uid')
        op = self.query.getfirst('op')
        submit = self.query.getfirst('submit')
        self.logger.debug('admin_user: uid=%s, op=%s, submit=%s', uid, op, submit)
        cur = self.db.cursor()
        roles = [f[0] for f in cur.execute('select distinct role from perm')]
        if submit and submit.lower() in ('cancel', 'no'):
            return Response(redirect='admin_user')
        elif ((op in ('add', 'edit') and submit and submit.lower() == 'save') or
              (op == 'delete' and submit and submit.lower() == 'yes')):
            tmplname = 'admin_user_%s' % op
            if self.command != 'POST':
                self.logger.error('must be POST, not GET')
                return Response(error=405)
            elif op in ('edit', 'delete') and uid is None:
                self.logger.error('missing fields for admin_user[%s]', op)
                self.session.error = 'Data missing'
                return Response(redirect='admin_user')
            name = self.query.getfirst('name')
            email = self.query.getfirst('email')
            userroles = self.query.getlist('role')
            if uid:
                myroles = [
                    f[0] for f in
                        cur.execute('select distinct name from role '
                            'where user = ?', (uid,))
                ]
            else:
                myroles = []
            rattrs = {
                'uid': uid,
                'roles': roles,
                'thisuserroles': userroles,
                'name': name,
                'email': email,
            }
            if op == 'delete' and uid == 1:
                self.logger.error('attempt to delete uid 1')
                self.session.error = 'Cannot delete %s' % name
                return Response(redirect='admin_user')
            elif op == 'edit' and uid == 1 and name != 'inquisitor':
                self.logger.error('attempt to change name of inquisitor user')
                self.session.error = 'Cannot change name of inquisitor'
                return Response(redirect='admin_user')
            elif op in ('add', 'edit') and name is None:
                self.logger.error('missing field for admin_user[%s]', op)
                self.session.error = 'Data missing'
                return Response(tmplname, rattrs)
            elif op in ('add', 'edit') and not self.valid_name(name):
                self.logger.error('invalid name: %s', repr(name))
                self.session.error = 'Invalid name, use /[A-Za-z][A-Za-z0-9_.-]/'
                return Response(tmplname, rattrs)
            else:
                cur.execute('select name, password, email from user where id = ?', (uid,))
                record = cur.fetchone()
            if record:
                sessions = cur.execute('select id from session where user = ?',
                                       (record[0],)).fetchall()
            else:
                sessions = []
            if op == 'delete' and record[1] == self.session.user:
                self.logger.error('cannot delete self')
                self.session.error = 'Cannot delete yourself'
                return Response(redirect='admin_user')
            try:
                with self.db as db:
                    if not record:
                        c = db.execute('insert into user '
                                '(name) values (?)',
                                (name,))
                        uid = c.lastrowid
                    elif op == 'delete':
                        db.execute('delete from user where id = ?', (uid,))
                    elif name != record[0]:
                        db.execute('update user set name = ? where id = ?',
                                (name, uid))
                    # update the email
                    if not record or email != record[2]:
                        db.execute('update user set email = ? where id = ?',
                                (email, uid))
                    # delete the password
                    if op == 'delete' and record[1]:
                        db.execute('delete from password where id = ?',
                                (record[1],))
                    toins = [(r, uid) for r in userroles if r not in myroles]
                    todel = [(r, uid) for r in myroles if r not in userroles]
                    db.executemany(
                        'insert into role values (?, ?)',
                        toins
                    )
                    db.executemany(
                        'delete from role where name = ? and user = ?',
                        todel
                    )
                    # if the user name changed, update the session
                    if op == 'edit' and record[0] != name:
                        self.logger.warning('changing user for %d sessions', len(sessions))
                        for sid in sessions:
                            self.logger.debug('old name = %s; new name = %s; sid = %s', record[0], name, sid[0])
                            db.execute(
                                'update session set user = ?, error = '
                                '"your username has changed" '
                                'where id = ?', (name, sid[0]))
                    # delete any open sessions for user being deleted
                    elif op == 'delete':
                        for sid in sessions:
                            db.execute('delete from session where id = ?',
                                       (sid[0],))
            except sqlite3.IntegrityError:
                self.logger.error('error adding %s to user table', name)
                self.session.error = 'User already exists'
            else:
                if op == 'add':
                    self.logger.info('new uid %s', uid)
                elif op == 'edit':
                    self.logger.info('save uid %s', uid)
                elif op == 'delete':
                    self.logger.info('delete uid %s', uid)
            return Response(redirect='admin_user')
        elif submit and submit.lower() in ('delete', 'edit', 'new'):
            if submit == 'New':
                tmplname = 'admin_user_add'
            else:
                tmplname = 'admin_user_%s' % str(submit).lower()
            allroles = {}
            for name, perm in cur.execute('select role, permid from perm'):
                if name in allroles:
                    allroles[name].append(perm)
                else:
                    allroles[name] = [perm]
            import json
            attrs = {
                'uid': uid,
                'name': None,
                'email': None,
                'roles': roles,
                'thisuserroles': None,
                'thisuserperms': None,
                'permjson': json.dumps(allroles),
            }
            cur.execute(
                'select name, password, email from user where id = ?',
                (uid,))
            f = cur.fetchone()
            if f is not None:
                # cannot delete inquisitor
                if uid == 1 and submit == 'Delete':
                    self.session.error = 'Cannot delete a god'
                    return Response(redirect='admin_user')
                # do not delete myself
                if f[0] == self.session.user and submit == 'Delete':
                    self.session.error = 'Cannot delete yourself'
                    return Response(redirect='admin_user')
                attrs['name'] = f[0]
                attrs['email'] = f[2] or ""
                attrs['thisuserroles'] = [
                    f[0] for f in cur.execute(
                        'select name from role where user = ?',
                        (uid,))
                ]
                attrs['thisuserperms'] = json.dumps([
                    str(f[0]) for f in cur.execute(
                        'select distinct p.permid from perm as p, role as r '
                        'where r.user = ? and r.name = p.role',
                        (uid,))
                ])
                if submit == 'Delete':
                    sql = ('select a.release, t.product, t.name '
                        'from assignment as a, testcase as t '
                        'where a.user = ? and a.test = t.tid '
                        'order by a.release, t.product, t.name')
                    attrs['tests'] = t = []
                    for f in cur.execute(sql, (uid,)):
                        t.append({'r': f[0], 'p': f[1], 'n': f[2]})
                return Response(tmplname, attrs)
            else:
                return Response('admin_user_add', attrs)
        else:
            rows = []
            for u in cur.execute('select id, name, email from user order by name'):
                rows.append({'uid': u[0], 'name': u[1], 'email': u[2] or '<no email>'})
            activeusers = []
            for f in cur.execute('select user, time, id from session'):
                if f[2] == self.session.id:
                    activeusers.append(
                        {'name': '&lt;me&gt;', 'time': time.ctime(f[1]), 'sessionid': f[2]}
                    )
                else:
                    activeusers.append(
                        {'name': f[0], 'time': time.ctime(f[1]), 'sessionid': f[2]}
                    )
            return Response('admin_user',
                    {'rows': rows, 'activeusers': activeusers})

    def call_forget(self):
        """There are four modes to the /forget url:
1. /forget; no smtp.from config - display message asking site admin to change the password for you
2. /forget; smtp.from config - display form with username or email, on submit, send email to email
        with generated key
3. /forget/key - display form asking for key, calls /forget?key={{key}} (sames as below)
4. /forget/{{key}} - verify data in db, redirect to /password?uid={{uid}}
The forget key expires after two hours."""
        # purge old entries before anything else
        sql = ('delete from forget where '
               'datetime(time, "2 hours") < datetime("now")')
        if self.config['smtp.from'] is None:
            return Response('forget', {'nosmtp': True})
        self.db.execute(sql)
        self.db.commit()
        cur = self.db.cursor()
        submit = self.query.getfirst('do_submit')
        email = self.query.getfirst('email', None)
        name = self.query.getfirst('name', None)
        params = {
            'nosmtp': False,    # no configuration set for forget to email
            'withparts': False, # parts after base url path
            'askkey': False,    # asking for a key to be entered
        }
        if self.parts:
            params['withparts'] = True
            key = None
            if self.parts[0] == 'login':
                return Response(redirect='../login')
            elif self.parts[0] == 'key':
                params['askkey'] = True
            else:
                key = self.parts[0]
        else:
            key = self.query.getfirst('key', None)
        # this is how many levels deep we may be
        if self.referer is None or len(self.parts) == 0:
            prefix = ''
        else:
            prefix = '../' * len(self.parts)
        self.logger.debug('forget: email=%s; key=%s; askkey=%s', email, key, params['askkey'])
        if submit == 'Cancel':
            return Response(redirect=prefix + "login")
        if key is not None:
            cur.execute('select uid, time from forget where key = ?', (key,))
            data = cur.fetchone()
            if not data:
                self.logger.warn('no forget data for key %s', key)
                params['error'] = 'no such key found'
                return Response('forget', params)
            uid, when = data
            now = time.time()
            if when < now:
                self.logger.info('forget entry expired, %s', key)
                params['error'] = 'Forget request expired'
                return Response('forget', params)
            with self.db as db:
                db.execute('delete from forget where key = ?', (key,))
            return Response(redirect=prefix + 'password?uid=%s' % uid)
        elif email is not None or name is not None:
            from uuid import UUID
            from smtplib import SMTP, SMTPException
            if email is not None:
                cur.execute('select id, name, email from user where email = ?', (email,))
            else:
                cur.execute('select id, name, email from user where name = ?', (name,))
            data = cur.fetchone()
            if not data:
                self.logger.warn('forget: no user for %s', email or name)
                params['error'] = 'Cannot reset password; contact admin'
                return Response('forget', params)
            uid, username, umail = data
            if umail is None:
                self.logger.warn('forget: no email for user %d', uid)
                params['error'] = 'Cannot reset password; contact admin'
                return Response('forget', params)
            # the key is just a random UUID, like the session key
            self.logger.debug('forget: found uid=%d, username=%s, email=%s', uid, username, umail)
            key = str(UUID(bytes=os.urandom(16)))
            with self.db as db:
                db.execute('insert into forget (key, uid) values (?, ?)',
                        (key, uid))
            # send email to the user's email with the key and link
            template = self.get_template('forget_email')
            fromaddr = self.config['smtp.from']
            emailparams = {
                'hostname': self.config['net.host'],
                'basepath': self.basepath,
                'from': fromaddr,
                'email': umail,
                'uid': uid,
                'name': username,
                'key': key,
            }
            page = template.render(emailparams)
            smtp = SMTP(self.config['smtp.server'], self.config['smtp.port'])
            # pass exception through to caller
            smtp.sendmail(fromaddr, [umail], page)
            params['askkey'] = True
            return Response('forget', params)
            #return Response(redirect='forget/key')
        else:
            return Response('forget', params)

    def call_help(self):
        from jinja2 import TemplateNotFound
        if len(self.parts) == 0:
            name = 'help/index'
        else:
            name = 'help/%s' % '/'.join(self.parts)
        try:
            template = self.get_template(name)
        except TemplateNotFound:
            try:
                template = self.get_template('%s/index' % name)
            except TemplateNotFound:
                self.logger.error('could not find template %s', name)
                return Response(error=404, page='template not found')
            else:
                return Response(template)
        else:
            return Response(template)

    def call_issues(self):
        release = self.session.release
        if release is None:
            return Response(redirect='release?redirect=issues')
        cur = self.db.cursor()
        i_id = None
        if 'submit' in self.query and 'ISSUES' not in self.perms:
            self.session.error = 'Permission denied'
        elif 'submit' in self.query:
            submit = self.query.getfirst('submit')
            if submit == 'New':
                return Response('issue')
            elif submit == 'Cancel':
                pass
            elif submit in ('Change', 'Save'):
                i_id = self.query.getfirst('id')
                owner = cur.execute(
                    'select id from user where name = ?',
                    (self.session.sudo or self.session.user,)).fetchone()[0]
                name = self.query.getfirst('name')
                test = self.query.getfirst('test')
                user = self.query.getfirst('user')
                info = self.query.getfirst('info')
                resolve = self.query.getfirst('resolve', '')
                try:
                    with self.db as db:
                        if submit == 'Change':
                            db.execute('update issue set name = ?, test = ?, '
                                       'user = ?, info = ?, resolution = ? '
                                       'where id = ? and release = ?',
                               (name, test, user, info, resolve, i_id, release))
                        else:
                            c = db.execute('insert into issue '
                                       'values (?, ?, ?, ?, ?, ?, ?, ?)',
                       (None, owner, name, release, test, user, info, resolve))
                            i_id = c.lastrowid
                        self.add_oplog(db, 'issues %s #%s' % (submit.lower(), i_id))
                except sqlite3.IntegrityError:
                    self.session.error = 'Duplicate entry'
                    return Response(redirect='issues')
                else:
                    return Response(redirect='issues?id=%s' % i_id)
        if i_id or 'id' in self.query:
            if i_id is None:
                i_id = self.query.getfirst('id')
            data = cur.execute(
                    'select * from issue where id = ? and release = ?',
                        (i_id, self.session.release)).fetchone()
            if data:
                uname = cur.execute('select name from user where id = ?',
                        (data[1],)).fetchone()[0]
                issue = {
                    'id': data[0], 'owner': uname, 'name': data[2],
                    'release': data[3], 'test': data[4] or '',
                    'user': data[5], 'info': data[6], 'resolve': data[7],
                }
            else:
                issue = None
        else:
            issue = None
        def statuscolor(resolution):
            if resolution:
                return 'testing'
            else:
                return 'failure'
        testcases = cur.execute(
                'select count(*) from testcase').fetchone()[0]
        names = [(n[0], n[1], statuscolor(n[2])) for n in
            cur.execute(
                'select id, name, resolution from issue where release = ?',
                    (self.session.release,))
        ]
        return Response('issues', {'names': names, 'issue': issue,
                                   'testcases': testcases})

    def call_login(self):
        username = self.query.getfirst('username')
        password = self.query.getfirst('password')
        if self.session is not None and self.session.user is not None:
            if self.session.release is not None:
                self.logger.warning('already logged in, redirect to "main"')
                return Response(redirect='main')
            else:
                self.logger.warning('already logged in, redirect to "release"')
                return Response(redirect='release')
        elif username is not None:  # username entered, not "initial" page
            if self.command == 'GET':
                return Response(error=405)
            # if no session, then we generate a new one
            if self.session is None:
                client, clientip = self.get_clientip()
                self.session = Session.generate(self.db, clientip)
                self.cookies['sessionid'] = self.session.id
                m = self.cookies['sessionid']
                m['path'] = self.basepath or '/'
            self.logger.info('checking username [and password]')
            cur = self.db.cursor()
            cur.execute('select id, password from user where name = ?',
                        (username,))
            f = cur.fetchall()
            errmsg = 'Invalid login attempt'
            if len(f) == 1:
                uid, pid = f[0]
                # if no password in db, then success iff no password offered
                # if password in db, then success iff hash matches offered password
                if password is not None and pid is not None:
                    cur.execute('select salt1, salt2, password from password where id = ?',
                            (pid,))
                    f = cur.fetchone()
                    if f:
                        salt1, salt2, encoded = f
                        if hashpassword(password, salt1, salt2) == encoded:
                            self.session.user = username
                            self.logger.info('login %s (uid %s)', username, uid)
                            return Response(redirect='release')
                        else:
                            self.logger.error('invalid password for %s', username)
                            self.session.error = errmsg
                            return Response('login')
                    else:
                        self.logger.error('invalid password attempt: %s', username)
                        self.session.error = errmsg
                        return Response('login')
                # no password in the db, force a password to be set
                elif pid is None:
                    self.logger.info('force password (uid %s)', uid)
                    return Response(redirect='password?uid=%s' % uid)
                #elif password is None and pid is None:
                #    self.session.user = username
                #    self.logger.info('login %s (uid %s)', username, uid)
                #    return Response(redirect='release')
                else:
                    self.logger.error('invalid password attempt: %s', username)
                    self.session.error = errmsg
                    return Response('login')
            else:
                self.logger.error('invalid user attempt: %s', username)
                self.session.error = errmsg
                return Response('login')
        # small race condition between the purge and now
        else:
            return Response('login')

    def call_logout(self):
        if 'sessionid' in self.cookies:
            self.invalidate_cookie('sessionid')
        if self.session is not None:
            self.session.clear()
            self.session = None
        return Response(redirect='login')

    def call_main(self):
        attrs = self.get_statistics()
        users, products = attrs['users'], attrs['products']
        user_fields, prod_fields = [], []
        def faildone(u):
            return u['fail'] and 'fail' or u['ytd'] == 0 and 'done' or ''
        for user in sorted(users):
            u = users[user]
            u['class'] = self.stateclass(None, faildone(u))
            user_fields.append(u)
        for prod in sorted(products):
            p = products[prod]
            p['class'] = self.stateclass(p['state'], faildone(p))
            prod_fields.append(p)
        attrs['issues'] = issues = []
        for issue in self.db.execute('select id, name, resolution from issue '
                'where release = ?', (self.session.release,)):
            issues.append({
                'id': issue[0], 'name': issue[1],
                'class': issue[2] and 'testing' or 'failure'})
        attrs['users'] = user_fields  # replace the dict
        attrs['products'] = prod_fields  # replace the dict
        return Response('dashboard', attrs)

    def call_mytests(self):
        if self.session.release is None:
            return Response(redirect='release?redirect=mytests')
        submit = self.query.getfirst('do_submit')
        lastuser = self.query.getfirst('lastuser')
        selectuser = self.query.getfirst('selectuser')
        selectprod = self.query.getfirst('selectprod', 'Product')
        if lastuser and selectuser != lastuser:
            selectprod = 'Product'
        if selectuser is None or selectuser == '----':
            selectuser = self.session.sudo or self.session.user
        if submit == 'Change':
            if 'PASSFAIL' not in self.perms:
                self.logger.error('no access to passfail')
                self.session.error = 'Permission denied'
            else:
                with self.db as db:
                    tid = self.query.getfirst('tid')
                    qval = self.query.getfirst('state')
                    self.add_oplog(db, 'mytests test #%s %s' % (tid, qval))
                    self.logger.debug('passfail: %s, %s', tid, qval)
                    if qval == '-':
                        db.execute('delete from passfail where tid = ? and release = ?',
                            (tid, self.session.release))
                    else:
                        db.execute('insert or replace into passfail values (?, ?, ?)',
                            (tid, self.session.release, qval))

        # if there were changes above, this would show the updated information
        # if there were no changes it would show the same
        fields = []
        cur = self.db.cursor()
        # return all of the test cases of the selectuser (or me) for this release
        # along with the product's state
        # we will get the test case's status separately since it is only stored
        # in the table when the status is not '-'
        t = 'select status from passfail where tid = ? and release = ?'
        self.logger.debug('viewing mytests as %s', selectuser)
        usql = ('select distinct user.name from user, assignment as a, '
            'testcase as t, release as r where r.name = ? and '
            'a.release = r.name and t.tid = a.test and a.user = user.id and '
            't.product = r.product'
        )
        users = [u[0] for u in cur.execute(usql, (self.session.release,))]
        if selectuser == 'All':
            sa = ('select t.tid, t.name, t.product, r.state, u.name from '
                'user as u, release as r, assignment as a, testcase as t '
                'where r.name = ? and u.id = a.user and '
                'a.release = r.name and r.product = t.product and '
                'a.test = t.tid'
            )
            data = (self.session.release,)
        else:
            sa = ('select t.tid, t.name, t.product, r.state, u.name from '
                'user as u, release as r, assignment as a, testcase as t '
                'where r.name = ? and u.name = ? and u.id = a.user and '
                'a.release = r.name and r.product = t.product and '
                'a.test = t.tid'
            )
            data = (self.session.release, selectuser)
        self.logger.debug('sa = %s; data = %s', sa, data)
        products = set()
        for f in cur.execute(sa, data):
            tid, name, product, state, user = f
            products.add(product)
            if selectprod != 'Product' and selectprod != product:
                continue
            # there is only an entry in passfail then the status is not '-'
            pcur = self.db.cursor()
            pcur.execute(t, (tid, self.session.release))
            f = pcur.fetchone()
            pcur.close()
            stateclass = self.stateclass(state, f and f[0] or None)
            disabled = self.is_disabled(state)
            statelabel = self.statelabel[state]
            attr = {'tid': tid, 'name': name, 'product': product,
                    'value': f and f[0] or '-', 'class': stateclass,
                    'disabled': disabled, 'label': statelabel,
                    'user': user,
            }
            self.logger.debug('%s', attr)
            fields.append(attr)
        fields.sort(key=lambda x: (x['product'], x['name']))
        validstates = (('-', '----'), ('pass', 'Pass'), ('fail', 'Fail'))
        users.insert(0, '----')
        if ((self.session.sudo or self.session.user) not in users and
                selectuser == (self.session.sudo or self.session.user)):
            selectuser = '----'
        users.append('All')
        return Response('mytests',
                {'rows': fields,
                 'users': users,
                 'selectuser': selectuser,
                 'states': validstates,
                 'products': list(products),
                 'selectprod': selectprod,
                })

    def call_oplog(self):
        if self.session.release is None:
            return Response('oplog', {'contents': []})
        cur = self.db.cursor()
        cur.execute('select user, op, time from oplog where release = ? '
                    'order by time',
                (self.session.release,))
        entries = [
            {'user': e[0], 'op': e[1], 'when': e[2]} for e in cur
        ]
        return Response('oplog', {'contents': entries})

    def call_password(self):
        cur = self.db.cursor()
        submit = self.query.getfirst('do_submit')
        uid = self.query.getfirst('uid', None)
        returnpage = self.query.getfirst('return', None)
        password1 = self.query.getfirst('password1', None)
        password2 = self.query.getfirst('password2', None)
        if not self.session or not self.session.user:
            returnpage = 'login'
        elif not returnpage and self.session and self.session.user:
            returnpage = self.referer
        if uid is None or submit == 'Cancel':
            return Response(redirect=returnpage)
        else:
            cur.execute('select name from user where id = ?', (uid,))
            row = cur.fetchone()
            if row:
                username = row[0]
            else:
                return Response(redirect=returnpage)
        data = {
            'uid': uid,
            'name': username,
            'return': returnpage,
        }
        # no password given
        if password1 is None or password2 is None:
            return Response('password', data)
        # error when either are blank
        elif password1 == '' or password2 == '':
            self.session.error = 'Supply a password (twice)'
            return Response('password', data)
        # passwords not equal
        elif password1 != password2:
            self.session.error = 'Password mismatch'
            return Response('password', data)
        # passwords equal
        elif password1 == password2:
            self.logger.debug('change password for uid = %s', repr(uid))
            self.change_password(uid, password1)
            return Response(redirect=returnpage)
        else:
            self.logger.warn('unknown condition on call_password; p1=%s, p2=%s', password1, password2)
            return Response('password', data)

    def call_release(self):
        cur = self.db.cursor()
        redirect = self.query.getfirst('redirect', 'main')
        if 'release' in self.query and 'submit' in self.query:
            if self.query.getfirst('submit') == 'Submit':
                val = self.query.getfirst('release')
                self.logger.warning('/release - %s', val)
                cur.execute('select distinct name from release where name = ?', (val,))
                f = cur.fetchall()
                self.logger.debug('f = %s', f)
                if len(f) == 1:
                    self.session.release = val
                    return Response(redirect=redirect)
                else:
                    self.session.error = 'Not a valid release'
            elif self.query.getfirst('submit') == 'Cancel':
                return Response(redirect='main')
        options = []
        numreleases = len(
            [r[0] for r in cur.execute('select distinct name from release')]
        )
        if numreleases == 0:
            if 'ADMIN' in self.perms:
                numtestcases = cur.execute(
                    'select count(*) from testcase').fetchone()[0]
                if numtestcases == 0:  # no real data
                    self.session.error = 'No test cases, create some'
                    return Response(redirect='admin_master')
                else:
                    self.session.error = 'No releases, create one'
                    return Response(redirect='admin_activations')
            else:
                return Response(redirect='main')
        sql = 'select distinct name from release order by name desc'
        options = [n[0] for n in cur.execute(sql)]
        return Response('release', {'release': self.session.release,
                                    'redirect': redirect,
                                    'options': options})

    def call_releng(self):
        if self.session.release is None:
            return Response(redirect='release?redirect=releng')
        def faildone(u):
            return u['fail'] and 'fail' or u['ytd'] == 0 and 'done' or ''
        cur = self.db.cursor()
        submit = self.query.getfirst('submit')
        if submit is not None:
            if 'RELEASE' not in self.perms:
                self.logger.error('no access to change releng')
                self.session.error = 'Permission denied'
            else:
                cur.execute('select product, state from release where name = ?',
                            (self.session.release,))
                entries = dict(cur.fetchall())
                with self.db:
                    for sname in [key for key in self.query if key.startswith('state_')]:
                        name = sname.replace('state_', '')
                        value = self.query.getfirst(sname)
                        if entries[name] != value:
                            self.add_oplog(self.db, 'releng %s %s' % (value, name))
                            self.logger.debug('name = %s; value = %s; was = %s',
                                    name, value, entries[name])
                            self.db.execute(
                                'update release set state = ? where name = ? and product = ?',
                                (value, self.session.release, name)
                            )
        attrs = self.get_statistics()
        prodstats = attrs['products']
        # continue and (re)display
        fields = []
        q = 'select product, state from release where name = ?'
        states = [(s, self.statelabel[s]) for s in self.statelabels]
        for product, state in cur.execute(q, (self.session.release,)):
            # if all testcases are done and we are in 'testing' state, then
            # automatically change to 'verified'
            try:
                stats = attrs['products'][product]
            except KeyError:
                pass
            else:
                if state == 'testing' and stats['ytd'] == 0 and stats['fail'] == 0:
                    self.db.execute('update release set state = "verified" '
                            'where name = ? and product = ? and '
                            'state = "testing"',
                            (self.session.release, product))
                    self.add_oplog(self.db, 'releng verified %s' % product)
                    self.db.commit()
                    state = 'verified'
            try:
                stateclass = self.stateclass(state,
                                faildone(prodstats[product]))
            except KeyError:  # noone assigned?
                stateclass = self.stateclass(state, None)
            prodattrs = {'stateclass': stateclass, 'product': product,
                     'curstate': state}
            fields.append(prodattrs)
        attrs['rows'] = fields
        attrs['states'] = states
        return Response('releng', attrs)

    def call_static(self):
        ims = self.headers.get('if-modified-since', None)
        inm = self.headers.get('if-none-match', None)
        try:
            filename = self.parts[0]
        except IndexError:
            return Response(error=404, page='no static file')
        else:
            path = os.path.join(self.staticdir, filename)
            if not os.path.isfile(path):
                return Response(error=404, page='no static file')
            if inm is not None:
                file_etag = gen_etag(path, {})
                if inm == file_etag:
                    return Response(error=304, page='Not modified etag')
            if ims is not None:
                length = None
                if ';' in ims:
                    date, length = ims.split('; ')
                    if length.startswith('length='):
                        length = int(length[7:])
                    ims = date
                try:
                    ims_time = time.strptime(ims, '%a %b %d %H:%M:%S %Y')
                except ValueError:  # ie has a different format
                    ims_time = time.strptime(ims, '%a, %d %b %Y %H:%M:%S %Z')
                file_mtime = time.gmtime(os.path.getmtime(path))
                file_size = os.path.getsize(path)
                if (ims_time < file_mtime and
                    (length is not None and length != file_size)):
                    return Response(error=304, page='Not modified mtime')
            if filename.endswith('.css'):
                ctype = 'text/css'
            elif filename.endswith('.js'):
                ctype = 'application/javascript'
            elif filename.endswith('.gif'):
                ctype = 'image/gif'
            elif filename.endswith('.html'):
                ctype = 'text/html'
            elif filename.endswith('.txt'):
                ctype = 'text/plain'
            else:
                ctype = 'application/octet-stream'
            return Response(static=filename, ctype=ctype)

    def call_test(self):
        format = self.query.getfirst('format', 'text')
        if format not in ('text', 'html'):
            format = 'text'
        cur = self.db.cursor()
        numusers = '0'
        lastapplied = ''
        try:
            cur.execute('select name from version order by applied desc')
            lastapplied = cur.fetchone()[0]
            cur.execute('select count(*) from session where user is not null')
            numusers = cur.fetchone()[0]
        except:
            status = 'failure'
            lastapplied = 'db error'
        else:
            status = 'success'
        starttime = (self.server_type == 'httpd' and
                     time.ctime(self.server.starttime) or
                     None)
        attrs = {
            'result': status,
            'releaseno': version,
            'dbvers': lastapplied,
            'numusers': numusers,
            'starttime': starttime,
        }
        if format == 'html':
            response = Response('smoketest_html', attrs,
                    ctype='text/html')
        else:
            response = Response('smoketest_text', attrs,
                    ctype='text/plain')
        return response

    def call_test_exception(self):
        raise AssertionError('This is to test the exception handler')

    def call_testinfo(self):
        tid = self.query.getfirst('tid')
        if tid is None:
            self.send_error(error=404, page='no such tid')
        else:
            urls, hosts = [], []
            cur = self.db.cursor()
            self.logger.debug('testinfo tid = %s', tid)
            cur.execute('select * from testcase where tid = ?', (tid,))
            f = cur.fetchone()
            if f is None:
                return Response('testinfo', {'error': "No such test case"})
            tid, name, product, info = f
            for dtype, data in cur.execute(
                    'select type, data from productinfo where product = ?',
                    (product,)):
                if dtype == 'url':
                    urls.append('<a href="%s">%s</a>' % (data, data))
                elif dtype == 'userhost':
                    hosts.append(data)
            attrs = {
                'tid': tid,
                'name': name,
                'product': product,
                'info': info,
                'urls': urls,
                'hosts': hosts,
            }
            return Response('testinfo', attrs)

    def call_user(self):
        cur = self.db.cursor()
        submit = self.query.getfirst('submit')
        if submit and submit.lower() == 'submit':
            if self.command != 'POST':
                return Response(error=405)
            uid = self.query.getfirst('uid')
            email = self.query.getfirst('email')
            if uid is not None:
                with self.db as db:
                    if email is not None:
                        self.logger.debug(
                            'changing email to %s for uid = %s',
                            repr(email), repr(uid))
                        db.execute('update user set email = ? where id = ?',
                            (email, uid))
        userinfo = cur.execute('select * from user where name = ?',
                        (self.session.sudo or self.session.user,)).fetchone()
        # userroles will be assigned later in process
        return Response('userinfo', {'uid': userinfo[0],
            'name': userinfo[1], 'email': userinfo[3] or ''})

def sanitize(d):
    """Mask passwords and do not pass through the current time (for hashing)."""
    nd = {}
    for k in d:
        if k.startswith('password'):
            nd[k] = '********'
        elif k != 'currenttime':
            nd[k] = d[k]
    return nd

