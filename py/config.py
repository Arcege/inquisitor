#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.config
#

import os

homedir = os.path.expanduser('~')

progfile = os.path.realpath(__file__)
pydir = os.path.dirname(progfile)
basedir = os.path.dirname(pydir)

class Config(dict):
    """Configuration properties used through-out the program.
Loading files, autoloading base files."""
    filename = os.path.join(homedir, '.inquisitor')
    defaults = (
        # some will be ignored in a cgi context
        ('dir.app', os.path.realpath(os.curdir)),
        ('dir.database', None),
        ('dir.dist', basedir),
        ('dir.log', None),
        ('dir.static', 'static'),
        ('dir.templates', 'templates'),
        ('file.cert', None),            # ignore cgi
        ('file.database', 'inquisitor.sqlite'),
        ('file.log', None),
        ('file.log.count', 4),          # backupCount - ignore cgi
        ('file.log.maxsize', 24*1024**2),  # 24MB - ignore cgi
        ('file.log.access', None),      # ignore cgi
        ('file.pid', None),             # ignore cgi
        ('log.level', 'WARNING'),
        ('net.addr', ''),               # ignore cgi
        ('net.host', 'http://localhost'),      # for smtp
        ('net.path', ''),
        ('net.port', None),             # ignore cgi
        ('net.ssl', 'false'),           # ignore cgi
        ('session.expiry', '8 hours'),
        ('session.maxnew', '15 minutes'),
        ('smtp.from', None),
        ('smtp.server', 'localhost'),
        ('smtp.port', 25),
        ('sys.daemon', 'false'),        # ignore cgi
        ('sys.profiling', 'false'),     # ignore cgi
        ('sys.threading', 'true'),      # ignore cgi
        ('sys.umask', None),            # ignore cgi
    )
    # filenames to automatically load
    loadfilenames = (filename,)
    def __init__(self, *args, **kwargs):
        super(Config, self).__init__(*args, **kwargs)
        self.update(self.defaults)
        self.autoload()
    def clear(self):
        super(Config, self).clear()
        self.update(self.defaults)
    def default(self, name):
        for key, value in self.defaults:
            if name == key:
                return value
        else:
            return None
    def autoload(self):
        for fn in self.loadfilenames:
            if os.path.isfile(fn):
                self.load(fn)
    def load(self, filename):
        with open(filename, 'rt') as cfile:
            for line in cfile:
                if hasattr(line, 'decode'):
                    eline = line.decode('ascii').rstrip()
                else:
                    eline = line.rstrip()
                if not eline or eline.startswith('#'):
                    continue
                elif '=' in eline:
                    var, val = eline.split('=', 1)
                    self[var.strip()] = val.strip()
    def check_paths(self):
        assert os.path.isdir(self['dir.app']), 'Missing dir.app directory'
        assert os.path.isdir(self['dir.dist']), 'Missing dir.dist directory'
        assert os.path.isdir(
                os.path.join(self['dir.app'], self['dir.database'] or os.curdir),
        ), 'Missing dir.database direcrory'
        assert os.path.isdir(
                os.path.join(self['dir.app'], self['dir.log'] or os.curdir),
        ), 'Missing dir.log directory'
        assert os.path.isdir(
                os.path.join(self['dir.dist'], self['dir.static']),
        ), 'Missing dir.static directory'
        assert os.path.isdir(
                os.path.join(self['dir.dist'], self['dir.templates']),
        ), 'Missing dir.templates directory'
        if self['file.cert']:
            assert os.path.isfile(
                os.path.join(self['dir.app'], self['file.cert']),
            ), 'Missing file.cert file'
        if not self['file.database']:
            dbfile = 'inquisitor.sqlite'
        elif self['file.database'].endswith('.sqlite'):
            dbfile = self['file.database']
        else:
            dbfile = self['file.database'] + '.sqlite'
        assert os.path.isfile(
                os.path.join(self['dir.app'], self['dir.database'] or os.curdir,
                             dbfile)
        ), 'Missing file.database file'
        # config['file.log'] may or may not exist
        # config['file.pid'] may or may not exist

