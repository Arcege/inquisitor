#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.session
#

import os
import time

# session management
class Session(object):
    def __init__(self, db, id, time, ipaddr, user=None, release=None,
                 error=None, sudo=None):
        super(Session, self).__setattr__('db', db)
        super(Session, self).__setattr__('id', id)
        super(Session, self).__setattr__('time', time)
        super(Session, self).__setattr__('ipaddr', ipaddr)
        super(Session, self).__setattr__('user', user)
        super(Session, self).__setattr__('release', release)
        super(Session, self).__setattr__('error', error)
        super(Session, self).__setattr__('sudo', sudo)
    def __repr__(self):
        return 'Session(%s, %s, %s, %s, %s, %s)' % (
                self.id, time.ctime(self.time), self.user, self.release,
                self.error, self.sudo)
    valid_keys = ('error', 'release', 'user', 'sudo')
    def __setattr__(self, attr, value):
        if attr in self.valid_keys:
            sql = 'update session set %s = ? where id = ?' % attr
            self.db.execute(sql, (value, self.id))
            self.db.commit()
            super(Session, self).__setattr__(attr, value)
        else:
            raise KeyError(attr)

    def update_time(self):
        now = time.time()
        super(Session, self).__setattr__('time', now)
        self.db.execute('update session set time = ? where id = ?',
                    (now, self.id))

    def clear(self):
        sid = self.id
        for attr in ('time', 'ipaddr', 'user', 'release', 'error', 'sudo'):
            super(Session, self).__setattr__(attr, None)
        self.db.execute('delete from session where id = ?', (sid,))
        self.db.commit()

    @classmethod
    def generate(cls, db, clientip):
        from uuid import UUID
        sessionid = str(UUID(bytes=os.urandom(16)))
        # create an empty session
        s_time = time.time()
        with db:
            db.execute('insert into session (id, time, ipaddr) values (?, ?, ?)',
                    (sessionid, s_time, clientip))
        return cls(db, sessionid, s_time, clientip)
    @classmethod
    def retrieve(cls, db, sessionid):
        cur = db.cursor()
        cur.execute('select * from session where id = ?',
                (sessionid,))
        f = cur.fetchone()
        if f is None:
            raise KeyError
        else:
            return cls(db, *f)
    @classmethod
    def purge(cls, db, config):
        # if no user is set, then timeout after 15 minutes
        # otherwise there is a 8 hour timeout
        expiry = config['session.expiry']
        maxnew = config['session.maxnew']
        def verify(dtime):
            parts = dtime.split()
            def check(value, type):
                try:     type(value)
                except:  return False
                else:    return True
            if len(parts) == 2:
                if parts[1] in ('days', 'hours', 'minutes', 'months', 'years'):
                    return check(parts[0], int)
                elif parts[1] == 'seconds':
                    return check(parts[0], float)
                elif parts[0] == 'weekday':
                    return check(parts[1], int)
                return False
            elif len(parts) == 3:
                return (
                    parts[:2] == ['start', 'of'] and
                    parts[2] in ('day', 'month', 'year')
                )
            else:
                return False
        if not verify(expiry):
            expiry = config.default('session.expiry')
        if not verify(maxnew):
            maxnew = config.default('session.maxnew')
        data = {'maxnew': maxnew, 'expiry': expiry}
        sql = ('delete from session where (user is null and '
               'datetime(time, "unixepoch", "%(maxnew)s") < '
               'datetime("now")) or (user is not null and '
               'datetime(time, "unixepoch", "%(expiry)s") < '
               'datetime("now"))' % data)
        db.execute(sql)
        db.commit()

