#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.inqlogging
#

import logging
import logging.handlers
import os
import sys

logging_initialized = False

def start_logging(config):
    """Initialize logging module, sending messages to appopriate location(s)."""
    global logging_initialized
    if logging_initialized:
        return
    logging_initialized = True
    level = logging.WARNING
    logdir = os.path.join(config['dir.app'], config['dir.log'] or os.curdir)
    filename = stream = None
    if config['file.log']:
        filename = os.path.join(logdir, config['file.log'])
        maxsize = int(config['file.log.maxsize'])
        backupCount = int(config['file.log.count'])
        handler = logging.handlers.RotatingFileHandler(filename,
                    maxBytes=maxsize, backupCount=backupCount)
    else:
        handler = logging.StreamHandler(sys.stdout)
    logger = logging.getLogger(None)
    if 'log.level' in config:
        if hasattr(logging, config['log.level']):
            level = getattr(logging, config['log.level'])
    logger.setLevel(level)
    handler.setFormatter(
        logging.Formatter('%(asctime)s %(threadName)s %(message)s')
    )
    logger.addHandler(handler)
    logger = logging.getLogger('access')
    try:
        maxsize = int(config['file.log.maxsize'])
    except:
        maxsize = 0
    if config['file.log.access']:
        fname = os.path.join(logdir, config['file.log.access'])
        handler = logging.handlers.RotatingFileHandler(fname,
                maxBytes=maxsize, backupCount=5)
        handler.setFormatter(
            logging.Formatter('%(asctime)s %(threadName)s %(message)s'))
        logger.addHandler(handler)


# inject the session id into the log
class SessionAdapter(logging.LoggerAdapter):
    def process(self, msg, kwargs):
        return '[%s] %s' % (self.extra['sessionid'], msg), kwargs

