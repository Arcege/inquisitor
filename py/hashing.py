#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.hashing
#

import hashlib
import os
try:
    import cPickle as pickle
except ImportError:
    import pickle
import random

import constants

extnum = 0

def gen_etag(filename, params):
    """Generate an ETag value based on the file's size and mtime
as well as the parameters.  Usually, this will result in the same
hashed value for the same template and parameters."""
    if filename is None:
        size = mtime = 0
    else:
        size = os.path.getsize(filename)
        mtime = os.path.getmtime(filename)
    d = params.copy()
    try:
        del d['currenttime']
    except KeyError:
        pass
    items = list(d.items())
    items.sort(key=lambda x: x[0])
    #global extnum
    #f = open('foobar.%s' % extnum, 'wt')
    #for i in items:
    #    f.write('%s\n' % (i,))
    #f.close()
    #extnum += 1
    s = pickle.dumps(items)
    tag = '%d-%d-%s' % (size, mtime, s)
    return hashlib.md5(tag.encode(constants.encoding)).hexdigest()

def hashpassword(cleartext, salt1, salt2):
    """Generate a hash of a cleartext password with a leading
and trailing salt."""
    f = '%d%s%d' % (salt1, cleartext, salt2)
    return hashlib.sha256(f.encode('utf-8')).hexdigest()

def genpassword(cleartext):
    """Generate new salts and the return the hashed password
using those salts."""
    salt1 = random.randrange(100,  99999999999)
    salt2 = random.randrange(1000, 999999999999)
    return (salt1, salt2, hashpassword(cleartext, salt1, salt2))

