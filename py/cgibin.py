#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.cgibin
#

import cgi
import logging
import os
import sys
import time

import inquisitor
import config
import inqlogging

progname = os.path.realpath(__file__)
pydir = os.path.dirname(progname)

class LocalConfig(config.Config):
    """ Look in ~/.inquisitor and 'local.cfg' in the same directory
as cgibin.py"""
    loadfilenames = config.Config.loadfilenames + (
        os.path.join(pydir, 'local.cfg'),
    )

class Handler(inquisitor.Inquisitor):
    server_type = 'cgi'
    server = None

    def __init__(self):
        global config
        inquisitor.Inquisitor.__init__(self, config)
        self.query = cgi.FieldStorage()
        self.command = os.environ['REQUEST_METHOD']
        self.path = os.environ.get('PATH_INFO', '')
        self.basepath = os.environ['SCRIPT_NAME']
        self.referer = os.environ.get('HTTP_REFERER', None)
        if 'REMOTE_HOST' in os.environ:
            hostname = os.environ['REMOTE_HOST']
        else:
            hostname = os.environ['REMOTE_ADDR']
        try:
            self.client_address = (hostname, int(os.environ['REMOTE_PORT']))
        except KeyError:
            self.client_address = (hostname, 0)
        # headers are non accessible in CGI
        self.headers = {}
        self.wfile = sys.stdout

    def get_cookie_string(self):
        return os.environ.get('HTTP_COOKIE', '')

    def send_response(self, code, msg=None):
        if msg:
            code = '%s %s' % (code, msg)
        self.send_header('Status', code)


if __name__ == '__main__':
    config = LocalConfig()

    config.check_paths()  # raises AssertionError

    inqlogging.start_logging(config)
    server = Handler()
    server.request_handler()

