#!/usr/bin/python -u
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.httpd
#

from __future__ import print_function

try:
    import http.server as BaseHTTPServer
except ImportError:
    import BaseHTTPServer
import logging
import optparse
import os
import sys
import traceback

import inquisitor  # load this way to allow for reload on signal
import config
import inqlogging
import response
import query

class Handler(BaseHTTPServer.BaseHTTPRequestHandler, inquisitor.Inquisitor):
    server_type = 'docs'
    root_redirect = '/help'

    def __init__(self, *args, **kwargs):
        global config
        inquisitor.Inquisitor.__init__(self, config)
        BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, *args, **kwargs)

    def get_cookie_string(self):
        return ''

    def log_message(self, format, *args):
        self.logger.error(format, *args)

    def do_HEAD(self):
        self.query = query.Query()
        if '?' in self.path:
            self.path, q = self.path.split('?', 1)
            self.query.load(q)
        # XXX for debugging only
        self.logger.debug('HEAD %s query = %s', self.path, self.query)
        self.request_handler()

    def do_GET(self):
        self.query = query.Query()
        if '?' in self.path:
            self.path, q = self.path.split('?', 1)
            self.query.load(q)
        # XXX for debugging only
        self.logger.debug('GET %s query = %s', self.path, self.query)
        self.request_handler()

    def process(self):
        # first we strip off any basepath, and add it later on redirects
        if self.path.startswith(self.basepath + '/'):
            self.path = self.path.replace(self.basepath + '/', '/')
        assert self.path.startswith('/')
        parts = self.path.lstrip('/').split('/')
        name = parts[0]
        self.parts = parts[1:]
        self.session = None
        if name not in ('static', 'help'):
            return response.Response(error=404)
        methodname = 'call_' + name
        try:
            method = getattr(self, methodname)
        except AttributeError:
            r = response.Response(error=404)
        else:
            self.logger.info('%s(%s)', methodname, self.query)
            try:
                r = method()
            except Exception:
                self.logger.exception('Exception')
                t, e, tb = sys.exc_info()  # thread specific
                template = self.get_template('exception')
                params = {
                    'lines': traceback.format_exception(t, e, tb),
                    'username': self.session and
                        (self.session.sudo or self.session.user) or '',
                    'release': self.session and self.session.release or '',
                    'nav': {'list': [], 'active': '', 'visible': 'hidden'},
                    'root': self.root_redirect,
                }
                page = template.render(params)
                self.send_response(500)
                if len(self.cookies) > 0:
                    self.wfile.write(self.cookies.output())
                    self.wfile.write('\r\n')
                self.send_header('Content-Type', 'text/html')
                #self.send_header('Connection', 'close')
                self.send_header('Content-Length', str(len(page)))
                self.end_headers()
                self.wfile.write(page.encode(self.encoding))
                return None
            else:
                # add basepath back on redirects
                if r.redirect and self.basepath:
                    r.redirect = '%s/%s' % (
                        self.basepath, r.redirect.lstrip('/')
                    )
                if r.template:
                    params = r.params
                    params['root'] = self.root_redirect
                    params['basepath'] = self.basepath
                    params['VersionStr'] = 'Inquisitor %s' % inquisitor.version
                    params['nav'] = n = {
                        'list': [],
                        'active': '',
                        'visible': 'visible'
                    }
                    # handle displaying errors:
                    if self.session is not None and self.session.error is not None:
                        params['error'] = self.session.error
                        self.session.error = None
                    params['template'] = r.template
                    if isinstance(r.template, str):
                        template = self.get_template(r.template)
                        tname = r.template
                    else:
                        template = r.template
                        tname = os.path.basename(template.filename)
                        params['template'] = os.path.splitext(tname)[0]
                    self.logger.info('template = %s; params = %s',
                            r.template, params)
                    page = template.render(params)
                    r.page = page.rstrip('\n\r') + '\r\n'
                return r

    def send_error(self, code, message=None):
        try:
            from http.server import _quote_html
        except ImportError:
            from BaseHTTPServer import _quote_html
        try:
            short, long = self.responses[code]
        except KeyError:
            short, long = '???', '???'
        message = message or short
        explain = long
        self.log_error("code %d, message %s", code, message)
        content = (self.error_message_format %
                {'code': code, 'message': _quote_html(message), 'explain': explain})
        self.send_response(code, message)
        self.send_header('Content-type', self.error_content_type)
        if code in (204, 304):
            self.send_header('Connection', self.want_keep_alive)
        else:
            self.send_header('Connection', 'close')
        self.end_headers()
        if self.command != 'HEAD' and code >= 200 and code not in (204, 304):
            self.wfile.write(content.encode('utf-8'))


class Httpd(BaseHTTPServer.HTTPServer):   # for test single threaded
    force_conn_close = True

def main(config):
    inqlogging.start_logging(config)
    addr = (config['net.addr'], int(config['net.port']))
    httpd = Httpd(addr, Handler)
    this_host, this_port = httpd.server_address
    if this_host == '0.0.0.0':
        from socket import gethostname
        this_host = gethostname()
    logging.warn('Connect to "http://%s:%s/help', this_host, this_port)
    httpd.serve_forever()


option_parser = optparse.OptionParser()
option_parser.add_option('--config', '-C', metavar='FILE',
        help='configuration file, after %s' % config.Config.filename)
option_parser.add_option('--port', '-p', metavar='INT', type='int',
        help='http or https port to use')

if __name__ == '__main__':
    config = config.Config()

    opts, args = option_parser.parse_args()
    if opts.config:
        config.load(opts.config)

    if opts.port:
        config['net.port'] = opts.port
    if 'net.port' not in config or not config['net.port']:
        config['net.port'] = '0'

    # validate the values in config before we start anythin
    config.check_paths()
    try:
        int(config['net.port'])
    except (ValueError, TypeError):
        raise AssertionError('expecting number for net.port')

    try:
        main(config)
    except KeyboardInterrupt:
        print()

