#!/usr/bin/python
# Michael P. Reilly (c) 2016 All rights reserved
# inquisitor.py.utest
#

import os
import sys
if sys.version_info.major < 3:
    try:
        from cStringIO import StringIO
    except ImportError:
        from StringIO import StringIO
else:
    from io import StringIO
import sqlite3

import inquisitor
import config
import query

def generate_data(**overrides):
    data = {
        'headers': {},
        'command': 'GET',
        'path': '',
        'basepath': '',
        'referer': None,
        'client_address': ("", 0),
        'query': '',
        'database': None,
        'cookies': '',
    }
    data.update(overrides)
    return data

class UTestConfig(config.Config):
    loadfilenames = ()
    defaults = config.Config.defaults + (
        ('dir.database', 'test'),
        ('dir.log', 'test'),
        ('file.database', 'test.sqlite'),
        ('file.log', 'test.log'),
    )

class Handler(inquisitor.Inquisitor):
    """
"""
    server_type = 'usertest'
    server = None

    def __init__(self, data=None, config=None, last=None):
        if config is None:
            config = UTestConfig()
        if isinstance(last, inquisitor.Inquisitor):
            newdata = generate_data(
                headers=last.headers,
                command=last.command,
                path=last.path,
                basepath=last.basepath,
                referer=last.referer,
                client_address=last.client_address,
                query=last.query,
                database=last.db,
                cookies=last.cookies,
            )
        else:
            newdata = generate_data()
        if data is not None:
            newdata.update(data)
        print 'data =', newdata
        super(Handler, self).__init__(config)
        self.query = query.Query()
        self.server = None
        self.headers = {}
        self.headers.update(newdata.get('headers', {}))
        self.command = newdata.get('command', 'GET')
        self.path = newdata.get('path', '')
        self.basepath = newdata.get('basepath', '')
        self.referer = newdata.get('referer',
                self.headers.get('referer', None))
        self.client_address = newdata.get('client_address', ("", 0))
        if 'query' in newdata:
            if isinstance(newdata['query'], query.Query):
                self.query.update(newdata['query'])
            else:
                self.query.load(newdata['query'])
        self.wfile = StringIO()
        cookies = newdata.get('cookies', '')
        if not isinstance(cookies, str):
            cookies = cookies.output(attrs=[], header='Cookie:')
        self.cookie_string = cookies
        self.request_handler()

    def get_cookie_string(self):
        return self.cookie_string

    def send_response(self, code, msg=None):
        if msg:
            code = '%s %s' % (code, msg)
        data = '%s %s\n' % (self.protocol_version, code)
        self.wfile.write(unicode(data))
        self.send_header('Server', self.server_version)

    def parse_wfile(self):
        import httplib
        self.wfile.seek(0)
        reqline = self.wfile.readline()
        headers = httplib.HTTPMessage(self.wfile, seekable=True)
        body = self.wfile.read()
        req = reqline.split()
        assert int(req[1]), 'http response code is not numeric'
        if len(req) < 3:
            code = int(req[1])
            req.append(httplib.responses[code])
        return {
            'httpver': req[0],
            'status': int(req[1]),
            'reason': req[2],
            'headers': headers,
            'body': body,
        }

def mkdir(path):
    """Recursive mkdir, same as 'mkdir -p'."""
    if os.path.exists(path):
        return
    dirname = os.path.dirname(path)
    if not os.path.isdir(dirname):
        mkdir(dirname)
    os.mkdir(path)

def generate_db(config):
    dbfile = os.path.join(
        config['dir.app'],
        config['dir.database'],
        config['file.database']
    )
    mkdir(os.path.dirname(dbfile))
    thisdir = os.path.dirname(__file__)
    rootdir = os.path.dirname(thisdir)
    sqldir = os.path.join(rootdir, 'sql')
    conn = sqlite3.connect(dbfile)
    for fname in ('schema.sql', 'initial.sql'):
        sqlfile = os.path.join(sqldir, fname)
        sqlcode = open(sqlfile, 'rt').read()
        try:
            conn.executescript(sqlcode)
        except sqlite3.OperationalError, e:
            pass
        except sqlite3.IntegrityError, e:
            if 'version.name' in str(e):
                pass
            else:
                pass
        else:
            pass #print "applied", sqlfile
    return conn

