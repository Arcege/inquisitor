-- Michael P. Reilly (c) 2016 All rights reserved
-- inquisitor.sql.initial

begin transaction;

-- Initial permissions
insert or replace into perm values ('god', 'ADMIN');
insert or replace into perm values ('god', 'ASSIGN');
insert or replace into perm values ('god', 'DATA');
insert or replace into perm values ('god', 'ISSUES');
insert or replace into perm values ('god', 'MASTER');
insert or replace into perm values ('god', 'RELEASE');
insert or replace into perm values ('god', 'ROLES');
insert or replace into perm values ('god', 'SUDO');
insert or replace into perm values ('god', 'USERS');

insert or replace into perm values ('admin', 'ADMIN');
insert or replace into perm values ('admin', 'ASSIGN');
insert or replace into perm values ('admin', 'MASTER');
insert or replace into perm values ('admin', 'RELEASE');
insert or replace into perm values ('admin', 'ROLES');
insert or replace into perm values ('admin', 'SUDO');
insert or replace into perm values ('admin', 'USERS');

insert or replace into perm values ('releng', 'ADMIN');
insert or replace into perm values ('releng', 'RELEASE');

insert or replace into perm values ('manager', 'ISSUES');
insert or replace into perm values ('manager', 'RELEASE');

insert or replace into perm values ('qalead', 'ADMIN');
insert or replace into perm values ('qalead', 'MASTER');
insert or replace into perm values ('qalead', 'ASSIGN');

insert or replace into perm values ('tester', 'PASSFAIL');

-- a password for the admin 'inquisitor' user as "changeme"
insert into password values
    (null,13812285999, 568936416575,
     '996b2000544dd0b39fcbe22ecf886a18348e71cfd46cd37fcef0a98634429fa6');
-- the 'inquisitor' user should always be id 1
insert or replace into user values
    (1, 'inquisitor', last_insert_rowid(), null);
insert or ignore into role values ('god', 1);

insert or abort into version (name) values ("0.5");

end transaction;
