-- Michael P. Reilly (c) 2016 All rights reserved
-- inquisitor.sql.schema

begin transaction;

-- represent a user
create table if not exists user (
    id      integer primary key asc,
    name    text not null,
    password    integer,
    email   text
);
create unique index if not exists i_user on user(name);

-- represent a hashed password, with two salts
create table if not exists password (
    id      integer primary key asc,
    salt1   integer not null,
    salt2   integer not null,
    password    text
);
create unique index if not exists i_password on password(id);

-- a role may have multiple permissions
-- for example
create table if not exists role (
    name    text not null,
    user    integer not null
);
create unique index if not exists i_role on role(name, user);

-- permid should be static program values
create table if not exists perm (
    role    text not null,
    permid  text not null
);
create unique index if not exists i_perm on perm(role, permid);

-- each test case should be unique
create table if not exists testcase (
    tid     integer primary key asc,
    name    text not null,
    product text not null,
    info    blob
);
-- mostly search by product
create unique index if not exists i_testcase on testcase(name, product);
create index if not exists i2_testcase on testcase(product);

-- set during a release by a tester
-- status is ('pass', 'fail')
-- if no row, then default is unset
create table if not exists passfail (
    tid     integer not null,
    release text not null,
    status  text default '-'
);
create unique index if not exists i_passfail on passfail(tid, release);

-- maps which products are active in a release
-- each product will have a state: testing, deploying, etc.
create table if not exists release (
    name    text not null,
    product text not null,
    state   text default 'notstarted' not null
);
create unique index if not exists i_release on release(name, product);

-- association between user and testcase
create table if not exists assignment (
    user    integer not null,
    test    integer not null,
    release text
);
create unique index if not exists i_assignment on assignment(test, release);

-- issues database
create table if not exists issue (
    id      integer primary key asc,
    owner   integer not null,
    name    text not null,
    release text not null,
    test    integer,
    user    text,
    info    text,
    resolution  text
);
create unique index if not exists i_issue on issue(release, name);

-- use user.name instead of user.id as user may be deleted later
create table if not exists oplog (
    release text not null,
    user    text not null,
    op      text not null,
    time    text default CURRENT_TIMESTAMP
);
create index if not exists i_oplog on oplog(release);

create table if not exists productinfo (
    id      integer primary key asc,
    product text not null,
    type    text not null,
    data    text not null
);
create index if not exists i_productinfo on productinfo(product);

-- forget password database
create table if not exists forget (
    key     text not null,
    uid     integer not null,
    time    real default CURRENT_TIMESTAMP
);
create unique index if not exists i_forget on forget(key);

create table if not exists session (
    id      text not null,
    time    real not null,
    ipaddr  text not null,
    user    text,
    release text,
    error   text,
    sudo    text
);
create unique index if not exists i_session on session(id);

create table if not exists version (
    name    text unique on conflict abort not null,
    applied text default CURRENT_TIMESTAMP
);

insert or ignore into version (name) values ('schema');
insert or ignore into version (name) values ('1');
insert or ignore into version (name) values ('2');
insert or ignore into version (name) values ('3');
insert or ignore into version (name) values ('4');
insert or ignore into version (name) values ('5');
insert or ignore into version (name) values ('6');
insert or ignore into version (name) values ('7');
insert or ignore into version (name) values ('8');
insert or ignore into version (name) values ('9');
insert or ignore into version (name) values ('10');

end transaction;
